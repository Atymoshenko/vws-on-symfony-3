$(function () {
	var $locale = $('.field-locale');
	$locale.on('change', function() {
		$(this).closest('form').find('input[data-locale],textarea[data-locale]').closest('.form-group').hide();
		$(this).closest('form').find('input[data-locale="'+$locale.val()+'"],textarea[data-locale="'+$locale.val()+'"]').closest('.form-group').show();
	});
	$locale.trigger('change');

	var $checkboxRadio = $('input[data-radio]');
	$checkboxRadio.attr('type', 'radio');
	$checkboxRadio.closest('.checkbox').removeClass('checkbox').addClass('radio');
	$checkboxRadio.on('click', function() {
		var dataRadio = $(this).attr('data-radio');
		$(this).closest('form').find('[data-radio="'+dataRadio+'"]').attr('checked', false);
		$(this).prop('checked', true);
	})
});