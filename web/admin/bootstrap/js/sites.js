$(function () {
	var $containerDomains = $('.containerDomains');
	$containerDomains.on('click', '.btn-delete', function() {
		$(this).closest('.containerDomain').remove();
	});

	var domainsIndex = $containerDomains.find('.containerDomain').length;
	$('.btn-add-domain').on('click', function() {
		var prototype = $containerDomains.attr('data-prototype');
		var domainNew = prototype.replace(/__name__/g, domainsIndex);
		$containerDomains.append(domainNew);
		domainsIndex++;
	})
});