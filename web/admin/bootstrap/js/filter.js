$(function () {
	$('.filter').on('click', '.btn-open', function() {
		$(this).closest('.table-filter').addClass('hide');
		$(this).closest('.filter').find('.btn-close').closest('.table-filter').removeClass('hide');
	});
	$('.filter').on('click', '.btn-close', function() {
		$(this).closest('.table-filter').addClass('hide');
		$(this).closest('.filter').find('.btn-open').closest('.table-filter').removeClass('hide');
	});
});