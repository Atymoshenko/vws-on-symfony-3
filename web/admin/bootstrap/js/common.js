$(function () {
	$(function () {
		$('[data-toggle="tooltip"]').tooltip()
	});
	$(".btn-filter-clear").on('click', function() {
		var $form = $(this).closest('form');
		$form.find('input').each(function () {
			$(this).removeAttr('value');
		});
		$form.find('select').each(function () {
			$(this).val('');
		});
	});
	$(window).resize(function() {
		onResize();
	});
	var onResize = function() {
		$('#footerContainer').text($(window).width());

		var $el = $('td.col-sex');
		var widthFull = $el.width();
		var marginLeftRight = parseInt($el.css('margin-left')) + parseInt($el.css('margin-right'));
		var paddingLeftRight = parseInt($el.css('padding-left')) + parseInt($el.css('padding-right'));
		var borderLeftRight = parseInt($el.css('border-left-width')) + parseInt($el.css('border-right-width'));
		$('.test-1').text('wF: ' + widthFull );
		$('.test-2').text( 'mLR:' + marginLeftRight );
		$('.test-3').text( 'pLR: ' + paddingLeftRight );
		$('.test-4').text( 'bLR: ' + borderLeftRight );
		$('.test-5').text( 'wI: ' +  (widthFull + marginLeftRight + paddingLeftRight + borderLeftRight + 1) );
	};
	onResize();
});