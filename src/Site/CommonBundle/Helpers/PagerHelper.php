<?php
namespace Site\CommonBundle\Helpers;

use Doctrine\ORM\Query;
use Doctrine\ORM\Tools\Pagination\Paginator;

class PagerHelper
{
	/** @var Paginator */
	private $paginator;

	/** @var int */
	private $itemsPerPage;

	/** @var int */
	private $itemsCountTotal;

	/** @var int */
	private $pagesCountTotal = 0;

	/** @var int */
	private $pageCurrent;

	/** @var int */
	private $showPagesPrevCount = 3;

	/** @var int */
	private $showPagesNextCount = 3;

	public function __construct(Query $query, int $itemsPerPage = 10, bool $useOutputWalkers = false)
	{
		if ($itemsPerPage < 1) {
			throw new \Exception('Items per page can not be lower then 1!');
		}

		$this->paginator = new Paginator($query);
		$this->itemsPerPage = $itemsPerPage;
		$this->paginator->setUseOutputWalkers($useOutputWalkers);
		$this->itemsCountTotal = $this->paginator->count();
		$this->pagesCountTotal = (int) ceil($this->itemsCountTotal / $this->itemsPerPage);
	}

	public function getItemsPerPage(): int
	{
		return $this->itemsPerPage;
	}

	public function getItemsCountTotal(): int
	{
		return $this->itemsCountTotal;
	}

	public function getPagesCountTotal(): int
	{
		return $this->pagesCountTotal;
	}

	public function getPageCurrent(): int
	{
		return $this->pageCurrent;
	}

	public function getItemsByPage($page = 1): array
	{
		$this->pageCurrent = (int) $page;
		if ($this->pageCurrent < 1) {
			$this->pageCurrent = 1;
		}
		if ($this->pageCurrent > $this->pagesCountTotal && $this->pagesCountTotal > 0) {
			$this->pageCurrent = $this->pagesCountTotal;
		}
		//$this->paginator->getQuery()->setHint(Query::HINT_FORCE_PARTIAL_LOAD, true);
		$this->paginator->getQuery()->setFirstResult($this->itemsPerPage * ($this->pageCurrent - 1))->setMaxResults($this->itemsPerPage);

		return $this->paginator->getQuery()->getResult();
	}

	public function getPaginationAsArray(): array
	{
		$result = [];

		if (!$this->pageCurrent) {
			throw new \Exception('First call getItemsByPage method.');
		}

		$pageStart = $this->pageCurrent - $this->showPagesPrevCount;
		$pageStart = $pageStart < 1 ? 1 : $pageStart;
		$pageEnd = $this->pageCurrent + $this->showPagesNextCount;
		$pageEnd = $pageEnd > $this->pagesCountTotal ? $this->pagesCountTotal : $pageEnd;

		$result[] = [
			'number' => 1,
			'label' => '',
			'status' => $pageStart > 1 ? '' : 'disabled',
		];

		for ($i = $pageStart; $i <= $pageEnd; $i++) {
			$result[] = [
				'number' => $i,
				'label' => $i,
				'status' => $i == $this->pageCurrent ? 'active' : '',
			];
		}

		$result[] = [
			'number' => $this->pagesCountTotal,
			'label' => '',
			'status' => $pageEnd < $this->pagesCountTotal ? '' : 'disabled',
		];

		return $result;
	}
}