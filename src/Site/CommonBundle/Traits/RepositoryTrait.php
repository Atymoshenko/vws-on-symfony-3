<?php
namespace Site\CommonBundle\Traits;

use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityRepository;
use Site\UserBundle\Entity\Page;
use Site\UserBundle\Entity\PageService;
use Site\UserBundle\Entity\User;
use Site\UserBundle\Entity\UserGroup;
use Site\UserBundle\Repository\PageRepository;
use Site\UserBundle\Repository\PageServiceRepository;
use Site\UserBundle\Repository\UserGroupRepository;
use Site\UserBundle\Repository\UserRepository;

trait RepositoryTrait
{
	abstract protected function em(): EntityManager;

	protected function save($entity = null, bool $flushAll = true)
	{
		if ($entity)
		{
			$this->em()->persist($entity);
		}
		$this->flush($entity, $flushAll);
	}

	protected function remove($entity, bool $flushAll = false)
	{
		$this->em()->remove($entity);
		$this->flush($entity, $flushAll);
	}

	private function flush($entity, bool $flushAll = false)
	{
		$this->em()->flush($flushAll ? null : $entity);
	}

	protected function getRepository(string $entityClass): EntityRepository
	{
		return $this->em()->getRepository($entityClass);
	}

	protected function getUserRepository(): UserRepository
	{
		/** @var UserRepository $repository */
		$repository = $this->getRepository(User::class);

		return $repository;
	}

	protected function getUserGroupRepository(): UserGroupRepository
	{
		/** @var UserGroupRepository $repository */
		$repository = $this->getRepository(UserGroup::class);

		return $repository;
	}

	protected function getPageRepository(): PageRepository
	{
		/** @var PageRepository $repository */
		$repository = $this->getRepository(Page::class);

		return $repository;
	}

	protected function getPageServiceRepository(): PageServiceRepository
	{
		/** @var PageServiceRepository $repository */
		$repository = $this->getRepository(PageService::class);

		return $repository;
	}
}