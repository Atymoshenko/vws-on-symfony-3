<?php
namespace Site\CommonBundle\Entity;

use Control\AdminBundle\Validators\UserValidator;
use Site\UserBundle\Entity\Sex;
use Site\UserBundle\Entity\UserGroup;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoder;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Validator\Context\ExecutionContextInterface;

abstract class AbstractUser implements UserInterface, \Serializable
{
	/**
	 * @var int
	 * @ORM\Column(name="id", type="integer")
	 * @ORM\Id
	 * @ORM\GeneratedValue(strategy="AUTO")
	 */
	protected $id;

	/**
	 * @var \DateTime
	 * @ORM\Column(name="dateCreate", type="datetime", nullable=false)
	 */
	protected $dateCreate;

	/**
	 * @var \DateTime
	 * @ORM\Column(name="dateUpdate", type="datetime")
	 */
	protected $dateUpdate;

	/**
	 * @var string
	 * @ORM\Column(name="emailPrimary", type="string", length=255, unique=true, nullable=false)
	 */
	protected $emailPrimary;

	/**
	 * @var string
	 * @ORM\Column(name="email", type="string", length=255, unique=true, nullable=false)
	 */
	protected $email;

	/**
	 * @var string
	 * @ORM\Column(name="password", type="string", nullable=false)
	 */
	protected $password;

	/**
	 * @ORM\ManyToMany(targetEntity="Site\UserBundle\Entity\UserGroup", inversedBy="users")
	 * @ORM\JoinTable(name="user_user_group",
	 *      joinColumns={@ORM\JoinColumn(name="userId", referencedColumnName="id")},
	 *      inverseJoinColumns={@ORM\JoinColumn(name="userGroupId", referencedColumnName="id", unique=true)}
	 *     )
	 */
	protected $userGroups;

	/**
	 * @var string|null
	 * @ORM\Column(name="sex", type="string", columnDefinition="ENUM('M','F')")
	 */
	protected $sexCode;

	/** @var Sex */
	protected $sex;

	/**
	 * @var bool
	 * @ORM\Column(name="removed", type="boolean", nullable=false)
	 */
	protected $removed = false;

	/**
	 * @var string
	 * @ORM\Column(name="description", type="text")
	 */
	protected $description;

	/** @var string */
	protected $salt = 'mySalt';

	public function __construct()
	{
		$this->role = new ArrayCollection();
		$this->userGroups = new ArrayCollection();
	}

	public function getId(): ?int
	{
		return $this->id;
	}

	public function getDateCreate(): \DateTime
	{
		return $this->dateCreate;
	}

	public function getDateUpdate(): ?\DateTime
	{
		return $this->dateUpdate;
	}

	public function getEmailPrimary(): string
	{
		return $this->emailPrimary;
	}

	public function getEmail(): ?string
	{
		return $this->email;
	}

	public function setEmail(string $email)
	{
		$this->email = $email;
	}

	public function getPassword(): ?string
	{
		return $this->password;
	}

	public function setPassword(?string $password)
	{
		$this->password = $password;
	}

	public function encodePassword(UserPasswordEncoder $encoder)
	{
		$this->password = $encoder->encodePassword($this, $this->password);
	}

	public function getUserGroups()
	{
		return $this->userGroups;
	}

	public function setUserGroups($userGroups)
	{
		$this->userGroups = $userGroups;
	}

	public function getSex(): ?Sex
	{
		return $this->sex;
	}

	public function setSex(?Sex $sex)
	{
		$this->sex = $sex;
		$this->sexCode = $this->sex ? $this->sex->getCode() : null;
	}

	public function isRemoved(): bool
	{
		return $this->removed;
	}

	public function setRemoved(bool $removed)
	{
		$this->removed = $removed;
	}

	public function getDescription(): ?string
	{
		return $this->description;
	}

	public function setDescription(?string $description)
	{
		$this->description = $description;
	}

	public function getUsername()
	{
		return $this->getEmail();
	}

	public function getRoles(): array
	{
		$result = [];

		/** @var UserGroup $userGroup */
		foreach ($this->getUserGroups() as $userGroup) {
			foreach ($userGroup->getRoles() as $role) {
				if (!Role::validate($role)) {
					//continue;
				}
				if (!in_array($role, $result)) {
					$result[] = $role;
				}
			}
		}

		return $result;
	}

	public function getSalt(): string
	{
		return $this->salt;
	}

	public function eraseCredentials()
	{
		// TODO: Implement eraseCredentials() method.
	}

	public function serialize()
	{
		return serialize([$this->id, $this->email, $this->password, $this->salt]);
	}

	public function unserialize($serialized)
	{
		list($this->id, $this->email, $this->password, $this->salt) = unserialize($serialized);
	}

	/**
	 * @ORM\PreFlush
	 */
	public function doOnPreFlush()
	{
		if (is_null($this->id)) {
			$this->dateCreate = new \DateTime('now');
			if (is_null($this->emailPrimary)) {
				$this->emailPrimary = $this->email;
			}
		}

		$this->dateUpdate = new \DateTime('now');
	}

	/**
	 * @ORM\PostLoad
	 */
	public function doOnPostLoad()
	{
		$this->sex = $this->sexCode ? new Sex($this->sexCode) : null;
	}

	/**
	 * @param ExecutionContextInterface $context
	 * @param mixed $payload
	 * @Assert\Callback
	 */
	public function validate(ExecutionContextInterface $context, $payload)
	{
		$validator = new UserValidator($context, $payload);
		$validator->validate();
	}
}