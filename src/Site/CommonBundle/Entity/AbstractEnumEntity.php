<?php
namespace Site\CommonBundle\Entity;

abstract class AbstractEnumEntity
{
	abstract static public function getAll(): array;

	static public function validate(string $code)
	{
		return in_array($code, static::getAll()) ? $code : null;
	}

}