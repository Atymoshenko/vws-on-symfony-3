<?php
namespace Site\CommonBundle\Entity;

class Role extends AbstractEnumEntity
{
	const ROLE_USER = 'ROLE_USER';
	const ROLE_ADMIN = 'ROLE_ADMIN';

	static public function getAll(): array
	{
		return [
			self::ROLE_USER,
			self::ROLE_ADMIN,
		];
	}

	static public function getAllForForm(): array
	{
		$result = [];

		foreach (self::getAll() as $value) {
			$result[$value] = $value;
		}

		return $result;
	}

}