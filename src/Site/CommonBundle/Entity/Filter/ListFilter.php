<?php
namespace Site\CommonBundle\Entity\Filter;

class ListFilter
{
	public function hasConditions(): bool
	{
		foreach (get_object_vars($this) as $var) {
			if (!is_null($var)) {
				return true;
			}
		}

		return false;
	}

	public function getConditions(): array
	{
		$result = [];

		foreach (get_object_vars($this) as $key => $val) {
			$result[$key] = $val;
		}

		return $result;
	}
}