<?php
namespace Site\MultiSiteBundle\Traits;

use Doctrine\Common\Collections\ArrayCollection;
use Site\CommonBundle\Traits\RepositoryTrait;
use Site\MultiSiteBundle\Entity\Site;
use Site\MultiSiteBundle\Entity\SiteDomain;
use Site\MultiSiteBundle\Entity\SiteMultiSiteUser;
use Site\MultiSiteBundle\Repository\SiteDomainRepository;
use Site\MultiSiteBundle\Repository\SiteRepository;
use Site\MultiSiteBundle\Repository\SiteMultiSiteUserRepository;

trait SiteMultiSiteRepositoryTrait
{
	use RepositoryTrait;

	protected function getSiteMultiSiteUserRepository(): SiteMultiSiteUserRepository
	{
		/** @var SiteMultiSiteUserRepository $repository */
		$repository = $this->getRepository(SiteMultiSiteUser::class);

		return $repository;
	}

	protected function getSiteRepository(): SiteRepository
	{
		/** @var SiteRepository $repository */
		$repository = $this->getRepository(Site::class);

		return $repository;
	}

	protected function getSiteDomainRepository(): SiteDomainRepository
	{
		/** @var SiteDomainRepository $repository */
		$repository = $this->getRepository(SiteDomain::class);

		return $repository;
	}

	protected function saveSite(Site $site, bool $flushAll = true, ArrayCollection $domainsOriginal = null): void
	{
		if (!is_null($domainsOriginal)) {
			/** @var SiteDomain $domainOriginal */
			foreach ($domainsOriginal as $domainOriginal) {
				if (!$site->getDomains()->contains($domainOriginal)) {
					$this->remove($domainOriginal);
				}
			}
		}
		$this->save($site, $flushAll);
	}
}