<?php

namespace Site\MultiSiteBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Site\UserBundle\Entity\User;

/**
 * @ORM\Entity(repositoryClass="Site\MultiSiteBundle\Repository\SiteHistoryRepository")
 * @ORM\Table(name="site_history")
 * @ORM\HasLifecycleCallbacks()
 */
class SiteHistory
{
	/**
	 * @var int
	 * @ORM\Column(name="id", type="integer")
	 * @ORM\Id
	 * @ORM\GeneratedValue(strategy="AUTO")
	 */
	private $id;

	/**
	 * @var Site
	 * @ORM\ManyToOne(targetEntity="Site")
	 * @ORM\JoinColumn(name="siteId", referencedColumnName="id")
	 */
	private $site;

	/**
	 * @var SiteHistory
	 * @ORM\ManyToOne(targetEntity="SiteHistory")
	 * @ORM\JoinColumn(name="fromSiteHistoryId", referencedColumnName="id")
	 */
	private $fromSiteHistory;

	/**
	 * @var User
	 * @ORM\ManyToOne(targetEntity="Site\UserBundle\Entity\User")
	 * @ORM\JoinColumn(name="userId", referencedColumnName="id")
	 */
	private $user;

	/**
	 * @var string|null
	 * @ORM\Column(name="environment", type="string", length=50)
	 */
	private $environmentCode;

	/**
	 * @var \DateTime
	 * @ORM\Column(name="dateExpire", type="datetime", nullable=false)
	 */
	private $dateExpire;

	/**
	 * @var User
	 * @ORM\ManyToOne(targetEntity="Site\UserBundle\Entity\User")
	 * @ORM\JoinColumn(name="modifiedUserId", referencedColumnName="id")
	 */
	private $modifiedUser;

	/**
	 * @var \DateTime
	 * @ORM\Column(name="dateModified", type="datetime", nullable=false)
	 */
	private $dateModified;

	public function __construct(Site $site, SiteMultiSiteUser $user)
	{
		$this->site = $site;
		$this->modifiedUser = $user;

		$this->user = $site->getUser();
		$this->environmentCode = $site->getEnvironment() ? $site->getEnvironment()->getCode() : null;
		$this->dateExpire = $site->getDateExpire();
	}

	public function getId(): int
	{
		return $this->id;
	}

	public function getSite(): Site
	{
		return $this->site;
	}

	public function getFromSiteHistory(): ?SiteHistory
	{
		return $this->fromSiteHistory;
	}

	public function setFromSiteHistory(?SiteHistory $fromSiteHistory)
	{
		$this->fromSiteHistory = $fromSiteHistory;
	}

	public function getUser(): SiteMultiSiteUser
	{
		return $this->user;
	}

	public function getEnvironmentCode(): ?string
	{
		return $this->environmentCode;
	}

	public function getDateExpire(): ?\DateTime
	{
		return $this->dateExpire;
	}

	public function isDifferent(Site $site): bool
	{
		if ($site->getUser()->getId() !== $this->getUser()->getId()) {
			return true;
		}
		if (($site->getEnvironment() && is_null($this->getEnvironmentCode())) || (!$site->getEnvironment() && !is_null($this->getEnvironmentCode())) || ($site->getEnvironment() && $site->getEnvironment()->getCode() !== $this->getEnvironmentCode())) {
			return true;
		}
		if ($site->getDateExpire() !== $this->getDateExpire()) {
			return true;
		}

		return false;
	}

	/**
	 * @ORM\PreFlush
	 */
	public function doOnPreFlush()
	{
		$this->dateModified = new \DateTime('now');
	}
}