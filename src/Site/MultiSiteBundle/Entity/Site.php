<?php
namespace Site\MultiSiteBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Validator\Context\ExecutionContextInterface;
use Control\AdminSiteBundle\Validators\SiteValidator;

/**
 * @ORM\Table(name="site")
 * @ORM\Entity(repositoryClass="Site\MultiSiteBundle\Repository\SiteRepository")
 * @ORM\HasLifecycleCallbacks()
 */
class Site
{
    /**
     * @var int
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var SiteMultiSiteUser
	 * @ORM\ManyToOne(targetEntity="SiteMultiSiteUser", inversedBy="sites")
	 * @ORM\JoinColumn(name="userId", referencedColumnName="id")
     */
    private $user;

    /** @var int */
	private $userId;

	/**
	 * @var string|null
	 * @ORM\Column(name="environment", type="string", length=50)
	 */
    private $environmentCode;

    /** @var Environment */
    private $environment;

    /**
     * @var \DateTime
     * @ORM\Column(name="dateCreated", type="datetime")
     */
    private $dateCreated;

    /**
     * @var \DateTime
     * @ORM\Column(name="dateExpire", type="datetime")
     */
    private $dateExpire;

	/**
	 * @var SiteDomain[]|ArrayCollection
	 * @ORM\OneToMany(targetEntity="SiteDomain", mappedBy="site", cascade={"persist", "remove"})
	 * @ORM\OrderBy({"id" = "ASC"})
	 */
    private $domains;

    /** @var string */
    private $databaseHost;

    /** @var string */
	private $databaseName;

	/** @var string */
	private $databaseUser;

	/** @var string */
	private $databasePassword;

    /** @var Site|null */
    private $sitePrevious;

    public function __construct()
	{
		$this->domains = new ArrayCollection();
	}

	public function __clone()
	{
		$this->domains = new ArrayCollection();
	}

	public function getId(): ?int
    {
        return $this->id;
    }

    public function getUser(): ?SiteMultiSiteUser
    {
        return $this->user;
    }

    public function setUser(SiteMultiSiteUser $user)
	{
		$this->user = $user;
	}

	public function getUserId(): ?int
	{
		return $this->userId;
	}

	public function setUserId(int $userId)
	{
		$this->userId = $userId;
	}

	public function getEnvironment(): ?Environment
	{
		return $this->environment;
	}

	public function setEnvironment(?Environment $environment)
	{
		$this->environment = $environment;
	}

    public function getDateCreated(): \DateTime
    {
        return $this->dateCreated;
    }

    public function setDateExpire(?\DateTime $dateExpire)
    {
        $this->dateExpire = $dateExpire;
    }

    public function getDateExpire(): ?\DateTime
    {
        return $this->dateExpire;
    }

	public function getDomains()
	{
		return $this->domains;
	}

	public function addDomain(SiteDomain $siteDomain)
	{
		$this->domains->add($siteDomain);
		$siteDomain->setSite($this);
	}

	public function removeDomain(SiteDomain $domain)
	{
		$this->domains->removeElement($domain);
		$domain->removeSite();
	}

	public function getDatabaseHost(): ?string
	{
		return $this->databaseHost;
	}

	public function setDatabaseHost(?string $databaseHost)
	{
		$this->databaseHost = $databaseHost;
	}

	public function getDatabaseName(): ?string
	{
		return $this->databaseName;
	}

	public function setDatabaseName(?string $databaseName)
	{
		$this->databaseName = $databaseName;
	}

	public function getDatabaseUser(): ?string
	{
		return $this->databaseUser;
	}

	public function setDatabaseUser(?string $databaseUser)
	{
		$this->databaseUser = $databaseUser;
	}

	public function getDatabasePassword(): ?string
	{
		return $this->databasePassword;
	}

	public function setDatabasePassword(?string $databasePassword)
	{
		$this->databasePassword = $databasePassword;
	}

	public function getSitePrevious(): ?Site
	{
		return $this->sitePrevious;
	}

	public function setSitePrevious(Site $sitePrevious)
	{
		$this->sitePrevious = clone $sitePrevious;
		foreach ($sitePrevious->getDomains() as $domainPrevious) {
			$this->sitePrevious->addDomain(clone $domainPrevious);
		}
	}

	/**
	 * @ORM\PreFlush
	 */
	public function doOnPreFlush()
	{
		$this->environmentCode = $this->environment ? $this->environment->getCode() : null;

		if (is_null($this->id)) {
			$this->dateCreated = new \DateTime('now');
		}
	}

	/**
	 * @ORM\PostLoad
	 */
	public function doOnPostLoad()
	{
		$this->environment = $this->environmentCode ? new Environment($this->environmentCode) : null;
	}

	/**
	 * @param ExecutionContextInterface $context
	 * @Assert\Callback
	 */
	public function validate(ExecutionContextInterface $context)
	{
		$validator = new SiteValidator($context);
		$validator->validate();
	}
}