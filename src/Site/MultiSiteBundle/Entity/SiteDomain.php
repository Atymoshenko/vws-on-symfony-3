<?php
namespace Site\MultiSiteBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Validator\Context\ExecutionContextInterface;
use Control\AdminSiteBundle\Validators\SiteDomainValidator;

/**
 * @ORM\Table(name="site_domain")
 * @ORM\Entity(repositoryClass="Site\MultiSiteBundle\Repository\SiteDomainRepository")
 * @ORM\HasLifecycleCallbacks()
 */
class SiteDomain
{
    /**
     * @var int
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var Site
	 * @ORM\ManyToOne(targetEntity="Site", inversedBy="domains")
	 * @ORM\JoinColumn(name="siteId", referencedColumnName="id")
     */
    private $site;

    /**
     * @var string
     * @ORM\Column(name="domain", type="string", length=255, unique=true)
     */
    private $domain;

    static public function convertDomainToFolder(string $domain): string
	{
		return preg_replace('~(\.|-)~', '_', $domain);
	}

	public function __clone()
	{
		$this->removeSite();
	}

	public function getId(): int
    {
        return $this->id;
    }

    public function getSite(): Site
    {
        return $this->site;
    }

    public function setSite(Site $site)
	{
		$this->site = $site;
	}

	public function removeSite()
	{
		$this->site = null;
	}

    public function setDomain(string $domain)
    {
        $this->domain = $domain;
    }

    public function getDomain(): ?string
    {
        return $this->domain;
    }

    public function getPrevious(): ?SiteDomain
	{
		if (is_null($this->getSite()->getSitePrevious())) {
			return null;
		}

		/** @var SiteDomain $domainPrevious */
		foreach ($this->getSite()->getSitePrevious()->getDomains() as $domainPrevious) {
			if ($this->getId() === $domainPrevious->getId()) {
				return $domainPrevious;
			}
		}

		return null;
	}

	/**
	 * @ORM\PreFlush
	 */
	public function doOnPreFlush()
	{
	}

	/**
	 * @param ExecutionContextInterface $context
	 * @Assert\Callback
	 */
	public function validate(ExecutionContextInterface $context)
	{
		$validator = new SiteDomainValidator($context);
		$validator->validate();
	}
}