<?php
namespace Site\MultiSiteBundle\Entity;

use Site\CommonBundle\Entity\AbstractEnumEntity;

class Environment extends AbstractEnumEntity
{
	const ENVIRONMENT_LOCAL_MULTISITE = 'LM';
	const ENVIRONMENT_REMOTE = 'R';

	/** @var string */
	private $code;

	public function __construct(string $code)
	{
		$this->code = $code;
		if (!self::validate($this->code)) {
			throw new \InvalidArgumentException('Code ' . htmlspecialchars($this->code) . ' is invalid.');
		}
	}

	public function getCode(): string
	{
		return $this->code;
	}

	public function getValue(): string
	{
		return self::getAllForForm()[$this->code];
	}

	static public function getAll(): array
	{
		return [
			self::ENVIRONMENT_LOCAL_MULTISITE,
			/*self::ENVIRONMENT_REMOTE,*/
		];
	}

	static public function getAllForForm(): array
	{
		return [
			self::ENVIRONMENT_LOCAL_MULTISITE => 'locale multisite',
			/*self::ENVIRONMENT_REMOTE => 'remote',*/
		];
	}

	static function getAllForFormAsEntities(): \ArrayObject
	{
		$result = new \ArrayObject();
		foreach (self::getAllForForm() as $code => $value) {
			$result->offsetSet($code, new Environment($code));
		}

		return $result;
	}

	public function isLocal(): bool
	{
		return $this->getCode() === self::ENVIRONMENT_LOCAL_MULTISITE;
	}
}