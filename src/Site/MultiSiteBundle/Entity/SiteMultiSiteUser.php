<?php
namespace Site\MultiSiteBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;
use Site\CommonBundle\Entity\AbstractUser;

/**
 * @ORM\Table(name="user")
 * @ORM\Entity(repositoryClass="Site\MultiSiteBundle\Repository\SiteMultiSiteUserRepository")
 * @ORM\HasLifecycleCallbacks()
 */
class SiteMultiSiteUser extends AbstractUser
{
	/**
	 * @var Site[]|ArrayCollection
	 * @ORM\OneToMany(targetEntity="Site", mappedBy="user", cascade={"persist"})
	 */
	private $sites;

	public function __construct()
	{
		parent::__construct();
		$this->sites = new ArrayCollection();
	}

	public function getSites(): ArrayCollection
	{
		return $this->sites;
	}

	public function addSite(Site $site)
	{
		$this->sites->add($site);
	}
}