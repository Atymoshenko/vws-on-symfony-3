<?php
namespace Site\MultiSiteBundle\Entity;

class Role extends \Site\CommonBundle\Entity\Role
{
	const ROLE_ADMIN_SITE = 'ROLE_ADMIN_SITE';

	static public function getAll(): array
	{
		return array_merge(parent::getAll(), [
			'ROLE_ADMIN_SITE',
		]);
	}

}