<?php
namespace Site\MultiSiteBundle\Repository;

use Control\AdminSiteBundle\Entity\Filter\SitesListFilter;

class SiteRepository extends \Doctrine\ORM\EntityRepository
{
	public function getFilteredListQuery(SitesListFilter $filter)
	{
		$qb = $this->createQueryBuilder('s');

		/*if ($filter->getName()) {
			$qb->andWhere('s.name LIKE :name')->setParameter('name', '%' . $filter->getName() . '%');
		}*/

		$qb->orderBy('s.id', 'DESC');

		return $qb->getQuery();
	}
}