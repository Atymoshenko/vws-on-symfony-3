<?php
namespace Site\MultiSiteBundle\Controller;

use Site\MultiSiteBundle\Traits\SiteMultiSiteRepositoryTrait;

class BaseController extends \Site\UserBundle\Controller\BaseController
{
	use SiteMultiSiteRepositoryTrait;
}