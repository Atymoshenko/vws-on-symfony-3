<?php
namespace Site\MultiSiteBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Site\MultiSiteBundle\Entity\Site;
use Site\MultiSiteBundle\Entity\SiteDomain;
use Site\MultiSiteBundle\Entity\SiteMultiSiteUser;

class SiteController extends BaseController
{
    /**
     * @Route("/site", name="_site")
     */
    public function indexAction()
    {
    	/** @var \Site\UserBundle\Entity\User $user */
    	$user = $this->getUser();
    	if ($user) {
    		/** @var SiteMultiSiteUser $siteMultiSiteUser */
			$siteMultiSiteUser = $this->getSiteMultiSiteUserRepository()->find($user);
		}
        return $this->render('SiteMultiSiteBundle:Default:index.html.twig');
    }
}