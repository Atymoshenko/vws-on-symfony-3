<?php
namespace Site\UserBundle\EntityTranslation;

use Doctrine\ORM\Mapping as ORM;
use Gedmo\Translatable\Entity\MappedSuperclass\AbstractPersonalTranslation;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Validator\Context\ExecutionContextInterface;
use AdminBundle\Validators\PageServiceTranslationValidator;

/**
 * @ORM\Entity
 * @ORM\Table(name="page_service_translation",
 *     uniqueConstraints={@ORM\UniqueConstraint(name="objectId_locale_field", columns={
 *         "objectId", "locale", "field"
 *     })}
 * )
 */
class PageServiceTranslation extends AbstractPersonalTranslation
{
	/**
	 * Convenient constructor
	 *
	 * @param string $locale
	 * @param string $field
	 * @param string $value
	 */
	public function __construct($locale, $field, $value)
	{
		$this->setLocale($locale);
		$this->setField($field);
		$this->setContent($value);
	}

	/**
	 * @ORM\ManyToOne(targetEntity="Site\UserBundle\Entity\PageService", inversedBy="translations")
	 * @ORM\JoinColumn(name="objectId", referencedColumnName="id", onDelete="CASCADE")
	 */
	protected $object;

	/**
	 * @param ExecutionContextInterface $context
	 * @Assert\Callback
	 */
	public function validate(ExecutionContextInterface $context)
	{
		$validator = new PageServiceTranslationValidator($context);
		$validator->validate();
	}
}