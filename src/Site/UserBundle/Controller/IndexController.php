<?php
namespace Site\UserBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;
use Site\UserBundle\Entity\Page;

class IndexController extends BaseController
{
	/**
	 * @Route("/{_locale}", requirements={"_locale": "([a-z]{2})?"})
	 */
	public function indexAction(Request $request)
	{
		/** @var Page $page */
		$page = $this->getPageRepository()->findOneBy(['path' => 'vws']);

		return $this->render('SiteUserBundle:Default/Index:index.html.twig', ['page' => $page]);
	}
}