<?php
namespace Site\UserBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;

class PageController extends BaseController
{
	/**
	 * @Route("/{_locale}/{_page}", name="_page")
	 */
	public function pageAction(Request $request)
	{
		$page = $this->getPageRepository()->findOneBy(['path' => $request->getPathInfo()]);
		if (!$page) {
			throw $this->createNotFoundException('Page not found.');
		}

		$params = [
			'page' => $page,
		];

		return $this->render('SiteUserBundle:Default/Page:index.html.twig', $params);
	}
}
