<?php
namespace Site\UserBundle\Entity;

use Site\CommonBundle\Entity\AbstractEnumEntity;

class Locale extends AbstractEnumEntity
{
	const ENGLISH = 'en';
	const UKRAINIAN = 'uk';
	const RUSSIAN = 'ru';

	const DEFAULT = self::ENGLISH;

	static public function getAll(): array
	{
		return [
			self::ENGLISH,
			self::UKRAINIAN,
			self::RUSSIAN,
		];
	}

	static public function getAllForForm(): array
	{
		$result = [];

		foreach (self::getAll() as $code) {
			$result[$code] = $code;
		}

		return $result;
	}
}