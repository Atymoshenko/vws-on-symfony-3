<?php
namespace Site\UserBundle\Entity;

use Site\CommonBundle\Entity\Role;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Validator\Context\ExecutionContextInterface;
use Control\AdminBundle\Validators\UserGroupValidator;

/**
 * @ORM\Table(name="user_group")
 * @ORM\Entity(repositoryClass="Site\UserBundle\Repository\UserGroupRepository")
 */
class UserGroup
{
    /**
     * @var int
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     * @ORM\Column(name="name", type="string", length=50)
     */
    private $name;

    /**
     * @var array
     * @ORM\Column(name="roles", type="simple_array")
     */
    private $roles = [];

	/**
	 * @ORM\ManyToMany(targetEntity="User", mappedBy="userGroups")
	 */
    private $users;

    public function __construct()
	{
		$this->users = new ArrayCollection();
	}

	public function getId(): int
    {
        return $this->id;
    }

    public function setName(string $name)
    {
        $this->name = $name;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setRoles(array $roles)
    {
        $this->roles = $roles;
    }

    public function getRoles(): array
    {
        return $this->roles;
    }

	public function addRole(string $role)
	{
		if (Role::validate($role) && !in_array($role, $this->roles)) {
			$this->roles[] = $role;
		}
	}

	public function getUsers()
	{
		return $this->users;
	}

	/**
	 * @param ExecutionContextInterface $context
	 * @Assert\Callback
	 */
	public function validate(ExecutionContextInterface $context)
	{
		$validator = new UserGroupValidator($context);
		$validator->validate();
	}
}