<?php
namespace Site\UserBundle\Entity;

use AdminBundle\Validators\PageServiceValidator;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Site\UserBundle\EntityTranslation\PageServiceTranslation;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Validator\Context\ExecutionContextInterface;

/**
 * @ORM\Entity(repositoryClass="Site\UserBundle\Repository\PageServiceRepository")
 * @ORM\Table(name="page_service")
 * @ORM\HasLifecycleCallbacks()
 * @Gedmo\TranslationEntity(class="Site\UserBundle\EntityTranslation\PageServiceTranslation")
 */
class PageService
{
    /**
     * @var int
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     * @ORM\Column(name="path", type="string", length=255, unique=true)
     */
    private $path;

	/**
	 * @var string
	 * @ORM\Column(name="name", type="string", length=255)
	 * @Gedmo\Translatable
	 */
	private $name;

	/**
	 * @var string
	 * @ORM\Column(name="content", type="text")
	 * @Gedmo\Translatable
	 */
	private $content;

	/**
	 * @var string
	 * @ORM\Column(name="metaTitle", type="string", length=255)
	 * @Gedmo\Translatable
	 */
	private $metaTitle;

	/**
	 * @var string
	 * @ORM\Column(name="metaKeywords", type="string", length=255)
	 * @Gedmo\Translatable
	 */
	private $metaKeywords;

	/**
	 * @var string
	 * @ORM\Column(name="metaDescription", type="string", length=255)
	 * @Gedmo\Translatable
	 */
	private $metaDescription;

	/**
	 * @Gedmo\Locale
	 * Used locale to override Translation listener`s locale
	 * this is not a mapped field of entity metadata, just a simple property
	 */
	protected $locale;

	/**
	 * @ORM\OneToMany(targetEntity="Site\UserBundle\EntityTranslation\PageServiceTranslation", mappedBy="object",
	 *   cascade={"persist", "remove"}
	 * )
	 */
	private $translations;

	public function __construct()
	{
		$this->translations = new ArrayCollection();
	}

	public function getTranslations()
	{
		return $this->translations;
	}

	public function addTranslation(PageServiceTranslation $t)
	{
		if (!$this->translations->contains($t)) {
			$this->translations[] = $t;
			$t->setObject($this);
		}
	}

	public function fillTranslations(array $locales)
	{
		$fieldLocaleMap = [
			'name' => [],
			'content' => [],
			'metaTitle' => [],
			'metaKeywords' => [],
			'metaDescription' => [],
		];

		/** @var PageServiceTranslation $translation */
		foreach ($this->getTranslations() as $translation) {
			$fieldLocaleMap[$translation->getField()][] = $translation->getLocale();
		}
		foreach ($fieldLocaleMap as $field => $fieldLocales) {
			foreach ($locales as $locale) {
				if (!in_array($locale, $fieldLocales)) {
					$translation = new PageServiceTranslation($locale, $field, '');
					$this->addTranslation($translation);
				}
			}
		}
	}

	public function setLocale(string $locale)
	{
		$this->locale = $locale;
	}

    public function getId(): int
    {
        return $this->id;
    }

    public function setPath(?string $path)
    {
        $this->path = $path;
    }

    public function getPath(): ?string
    {
        return $this->path;
    }

	public function getName(): ?string
	{
		return $this->name;
	}

	public function setName(?string $name)
	{
		$this->name = $name;
	}

	public function getContent(): ?string
	{
		return $this->content;
	}

	public function setContent(?string $content)
	{
		$this->content = $content;
	}

	public function getMetaTitle(): ?string
	{
		return $this->metaTitle;
	}

	public function setMetaTitle(?string $metaTitle)
	{
		$this->metaTitle = $metaTitle;
	}

	public function getMetaKeywords(): ?string
	{
		return $this->metaKeywords;
	}

	public function setMetaKeywords(?string $metaKeywords)
	{
		$this->metaKeywords = $metaKeywords;
	}

	public function getMetaDescription(): ?string
	{
		return $this->metaDescription;
	}

	public function setMetaDescription(?string $metaDescription)
	{
		$this->metaDescription = $metaDescription;
	}

	/**
	 * @param ExecutionContextInterface $context
	 * @Assert\Callback
	 */
	public function validate(ExecutionContextInterface $context)
	{
		$validator = new PageServiceValidator($context);
		$validator->validate();
	}
}