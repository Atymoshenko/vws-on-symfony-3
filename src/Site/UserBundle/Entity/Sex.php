<?php
namespace Site\UserBundle\Entity;

use Site\CommonBundle\Entity\AbstractEnumEntity;

class Sex extends AbstractEnumEntity
{
	/** @var string */
	private $code;

	public function __construct(string $code)
	{
		$this->code = $code;
		if (!self::validate($this->code)) {
			throw new \InvalidArgumentException('Code ' . htmlspecialchars($this->code) . ' is invalid.');
		}
	}

	public function __toString(): string
	{
		return $this->getValue();
	}

	public function getCode(): string
	{
		return $this->code;
	}

	public function getValue(): string
	{
		return self::getAllForForm()[$this->code];
	}

	static public function getAll(): array
	{
		return ['M','F'];
	}

	static public function getAllForForm(): array
	{
		return [
			'M' => 'male',
			'F' => 'female',
		];
	}

	static function getAllForFormAsEntities(): \ArrayObject
	{
		$result = new \ArrayObject();
		foreach (self::getAllForForm() as $code => $value) {
			$result->offsetSet($code, new Sex($code));
		}

		return $result;
	}
}