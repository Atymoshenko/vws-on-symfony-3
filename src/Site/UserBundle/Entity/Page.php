<?php
namespace Site\UserBundle\Entity;

use Control\AdminBundle\Validators\PageValidator;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Site\UserBundle\EntityTranslation\PageTranslation;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Validator\Context\ExecutionContextInterface;

/**
 * @ORM\Entity(repositoryClass="Site\UserBundle\Repository\PageRepository")
 * @ORM\Table(name="page")
 * @ORM\HasLifecycleCallbacks()
 * @Gedmo\TranslationEntity(class="Site\UserBundle\EntityTranslation\PageTranslation")
 */
class Page
{
    /**
     * @var int
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     * @ORM\Column(name="path", type="string", length=255, unique=true)
     */
    private $path;

	/**
	 * @var bool
	 * @ORM\Column(name="active", type="boolean", nullable=false)
	 */
	private $active = false;

	/**
	 * @var bool
	 * @ORM\Column(name="visible", type="boolean", nullable=false)
	 */
	private $visible = true;

	/**
	 * @var string
	 * @ORM\Column(name="name", type="string", length=255)
	 * @Gedmo\Translatable
	 */
	private $name;

	/**
	 * @var string
	 * @ORM\Column(name="content", type="text")
	 * @Gedmo\Translatable
	 */
	private $content;

	/**
	 * @var string
	 * @ORM\Column(name="metaTitle", type="string", length=255)
	 * @Gedmo\Translatable
	 */
	private $metaTitle;

	/**
	 * @var string
	 * @ORM\Column(name="metaKeywords", type="string", length=255)
	 * @Gedmo\Translatable
	 */
	private $metaKeywords;

	/**
	 * @var string
	 * @ORM\Column(name="metaDescription", type="string", length=255)
	 * @Gedmo\Translatable
	 */
	private $metaDescription;

	/**
	 * @var User
	 * @ORM\ManyToOne(targetEntity="User")
	 * @ORM\JoinColumn(name="createdUserId", referencedColumnName="id")
	 */
	private $createdUser;

	/**
	 * @var \DateTime
	 * @ORM\Column(name="dateCreated", type="datetime", nullable=false)
	 */
	private $dateCreated;

	/**
	 * @Gedmo\Locale
	 * Used locale to override Translation listener`s locale
	 * this is not a mapped field of entity metadata, just a simple property
	 */
	protected $locale;

	/**
	 * @ORM\OneToMany(targetEntity="Site\UserBundle\EntityTranslation\PageTranslation", mappedBy="object",
	 *   cascade={"persist", "remove"}
	 * )
	 */
	private $translations;

	public function __construct()
	{
		$this->translations = new ArrayCollection();
	}

	public function getTranslations()
	{
		return $this->translations;
	}

	public function addTranslation(PageTranslation $t)
	{
		if (!$this->translations->contains($t)) {
			$this->translations[] = $t;
			$t->setObject($this);
		}
	}

	public function fillTranslations(array $locales)
	{
		$fieldLocaleMap = [
			'name' => [],
			'content' => [],
			'metaTitle' => [],
			'metaKeywords' => [],
			'metaDescription' => [],
		];

		/** @var PageTranslation $translation */
		foreach ($this->getTranslations() as $translation) {
			$fieldLocaleMap[$translation->getField()][] = $translation->getLocale();
		}
		foreach ($fieldLocaleMap as $field => $fieldLocales) {
			foreach ($locales as $locale) {
				if (!in_array($locale, $fieldLocales)) {
					$pageTranslation = new PageTranslation($locale, $field, '');
					$this->addTranslation($pageTranslation);
				}
			}
		}
	}

	public function setLocale(string $locale)
	{
		$this->locale = $locale;
	}

    public function getId(): int
    {
        return $this->id;
    }

    public function setPath(?string $path)
    {
        $this->path = $path;
    }

    public function getPath(): ?string
    {
        return $this->path;
    }

	public function isActive(): bool
	{
		return $this->active;
	}

	public function setActive(bool $active)
	{
		$this->active = $active;
	}

	public function isVisible(): bool
	{
		return $this->visible;
	}

	public function setVisible(bool $visible)
	{
		$this->visible = $visible;
	}

	public function getName(): ?string
	{
		return $this->name;
	}

	public function setName(?string $name)
	{
		$this->name = $name;
	}

	public function getContent(): ?string
	{
		return $this->content;
	}

	public function setContent(?string $content)
	{
		$this->content = $content;
	}

	public function getMetaTitle(): ?string
	{
		return $this->metaTitle;
	}

	public function setMetaTitle(?string $metaTitle)
	{
		$this->metaTitle = $metaTitle;
	}

	public function getMetaKeywords(): ?string
	{
		return $this->metaKeywords;
	}

	public function setMetaKeywords(?string $metaKeywords)
	{
		$this->metaKeywords = $metaKeywords;
	}

	public function getMetaDescription(): ?string
	{
		return $this->metaDescription;
	}

	public function setMetaDescription(?string $metaDescription)
	{
		$this->metaDescription = $metaDescription;
	}

	public function getCreatedUser(): ?User
	{
		return $this->createdUser;
	}

	public function setCreatedUser(User $createdUser)
	{
		$this->createdUser = $createdUser;
	}

	public function getDateCreated(): \DateTime
	{
		return $this->dateCreated;
	}

	/**
	 * @ORM\PreFlush
	 */
	public function doOnPreFlush()
	{
		if (is_null($this->id)) {
			$this->dateCreated = new \DateTime('now');
		}
	}

	/**
	 * @param ExecutionContextInterface $context
	 * @Assert\Callback
	 */
	public function validate(ExecutionContextInterface $context)
	{
		$validator = new PageValidator($context);
		$validator->validate();
	}
}