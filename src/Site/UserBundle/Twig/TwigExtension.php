<?php
namespace Site\UserBundle\Twig;

use Symfony\Component\HttpFoundation\RequestStack;

class TwigExtension extends \Twig_Extension
{
	/** @var RequestStack */
	protected $requestStack;

	public function __construct(RequestStack $requestStack)
	{
		$this->requestStack = $requestStack;
	}

	public function getFunctions()
	{
		return [
			new \Twig_SimpleFunction('getControllerName', [$this, 'getControllerName']),
			new \Twig_SimpleFunction('getActionName', [$this, 'getActionName']),
			new \Twig_SimpleFunction('getDate', [$this, 'getDate']),
		];
	}


	public function getControllerName(): string
	{
		$request = $this->requestStack->getCurrentRequest();
		if (!is_null($request)) {
			return preg_replace("~^.+\\\(.+?)Controller::.+?$~", "$1", $request->get('_controller'));
		}

		return '';
	}

	public function getActionName(): string
	{
		$request = $this->requestStack->getCurrentRequest();
		if (!is_null($request)) {
			return preg_replace("~^.*::(.+?)Action$~", "$1", $request->get('_controller'));
		}

		return '';
	}

	public function getDate(string $format = null): string
	{
		$date = new \DateTime();

		return $date->format($format);
	}
}