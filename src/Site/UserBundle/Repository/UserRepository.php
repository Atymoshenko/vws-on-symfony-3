<?php
namespace Site\UserBundle\Repository;

use Control\AdminBundle\Entity\Filter\UsersListFilter;

class UserRepository extends \Doctrine\ORM\EntityRepository
{
	public function getFilteredListQuery(UsersListFilter $filter)
	{
		$qb = $this->createQueryBuilder('u');

		if ($filter->getEmail()) {
			$qb->andWhere('u.email LIKE :email')->setParameter('email', '%' . $filter->getEmail() . '%');
		}
		if ($filter->getSex()) {
			$qb->andWhere('u.sexCode = :sexCode')->setParameter('sexCode', $filter->getSex());
		}
		if (!$filter->getWithRemoved()) {
			$qb->andWhere('u.removed = 0');
		}

		$qb->orderBy('u.id', 'DESC');

		return $qb->getQuery();
	}
}
