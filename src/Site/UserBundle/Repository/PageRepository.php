<?php
namespace Site\UserBundle\Repository;

use Control\AdminBundle\Entity\Filter\PagesListFilter;

class PageRepository extends \Doctrine\ORM\EntityRepository
{
	public function getFilteredListQuery(PagesListFilter $filter)
	{
		$qb = $this->createQueryBuilder('p');

		if ($filter->getName()) {
			$qb->andWhere('p.name LIKE :name')->setParameter('name', '%' . $filter->getName() . '%');
		}

		$qb->orderBy('p.id', 'DESC');

		return $qb->getQuery();
	}
}
