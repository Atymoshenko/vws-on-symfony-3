<?php
namespace Site\UserBundle\EventListener;

use Symfony\Component\DependencyInjection\Container;
use Symfony\Component\HttpKernel\Event\GetResponseForExceptionEvent;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\HttpExceptionInterface;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Site\UserBundle\Controller\PageController;

class ExceptionListener
{
	/** @var Container */
	private $container;

	public function __construct(Container $container)
	{
		$this->container = $container;
	}

	public function onKernelException(GetResponseForExceptionEvent $event)
	{
		if (preg_match("~^/admin[^a-z]~i", $event->getRequest()->getPathInfo())) {
			return;
		}
		// You get the exception object from the received event
		$exception = $event->getException();
		if ($exception instanceof NotFoundHttpException) {
			$controller = new PageController();
			$controller->setContainer($this->container);
			$response = $controller->pageAction($event->getRequest());
			$event->setResponse($response);
		}
		/*$message = sprintf('My Error says: %s with code: %s', $exception->getMessage(), $exception->getCode());

		// Customize your response object to display the exception details
		$response = new Response();
		$response->setContent($message);

		// HttpExceptionInterface is a special type of exception that
		// holds status code and header details
		if ($exception instanceof HttpExceptionInterface) {
			$response->setStatusCode($exception->getStatusCode());
			$response->headers->replace($exception->getHeaders());
		} else {
			$response->setStatusCode(Response::HTTP_INTERNAL_SERVER_ERROR);
		}

		// Send the modified response object to the event
		$event->setResponse($response);*/
	}
}