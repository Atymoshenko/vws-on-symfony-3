<?php
namespace Site\UserBundle\EventListener;

use Symfony\Component\HttpKernel\Event\GetResponseEvent;
use Symfony\Component\HttpKernel\KernelEvents;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Site\UserBundle\Entity\Locale;

class LocaleListener implements EventSubscriberInterface
{
	private $defaultLocale;

	public function __construct($defaultLocale = Locale::DEFAULT)
	{
		$this->defaultLocale = $defaultLocale;
	}

	public function onKernelRequest(GetResponseEvent $event)
	{
		$request = $event->getRequest();
		if (!$request->hasPreviousSession()) {
			return;
		}

		// try to see if the locale has been set as a _locale routing parameter
		if ($locale = $request->attributes->get('_locale')) {
			$request->getSession()->set('_locale', $locale);
		} else {
			// if no explicit locale has been set on this request, use one from the session
			$request->setLocale($request->getSession()->get('_locale', $this->defaultLocale));
		}
	}

	public static function getSubscribedEvents()
	{
		return array(// must be registered after the default Locale listener
			KernelEvents::REQUEST => array(array('onKernelRequest', 15)),);
	}
}