<?php
namespace Control\AdminBundle\Security;

use Site\CommonBundle\Traits\RepositoryTrait;
use Doctrine\ORM\EntityManager;
use Symfony\Component\Security\Core\Exception\UnsupportedUserException;
use Symfony\Component\Security\Core\Exception\UsernameNotFoundException;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Security\Core\User\UserProviderInterface;
use Site\UserBundle\Entity\User;

class UserProvider implements UserProviderInterface
{
	use RepositoryTrait;

	/** @var EntityManager */
	private $em;

	protected function em(): EntityManager
	{
		return $this->em;
	}

	public function __construct(EntityManager $em)
	{
		$this->em = $em;
	}

	public function loadUserByUsername($username)
	{
		/** @var User $user */
		$user = $this->getUserRepository()->findOneBy(['email' => $username]);
		if (!$user) {
			throw new UsernameNotFoundException(sprintf('Username "%s" does not exist.', $username));
		}

		return $user;
	}

	public function refreshUser(UserInterface $user)
	{
		if (!$user instanceof User) {
			throw new UnsupportedUserException(sprintf('Instances of "%s" are not supported.', get_class($user)));
		}

		return $this->loadUserByUsername($user->getUserName());
	}

	public function supportsClass($class)
	{
		return ($class === User::class);
	}
}