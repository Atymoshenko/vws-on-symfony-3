<?php
namespace Control\AdminBundle\Validators;

use Symfony\Component\Validator\Context\ExecutionContextInterface;
use Site\UserBundle\EntityTranslation\PageTranslation;

class PageTranslationValidator
{
	/** @var ExecutionContextInterface */
	private $context;

	public function __construct(ExecutionContextInterface $context)
	{
		$this->context = $context;
	}

	public function validate()
	{
		/** @var PageTranslation $pageTranslation */
		$pageTranslation = $this->context->getObject();

		if ($pageTranslation->getField() == 'name') {
			if (!$pageTranslation->getContent()) {
				$this->context->buildViolation("Name can't be empty.")->atPath('content')->addViolation();
			}
		}
	}
}