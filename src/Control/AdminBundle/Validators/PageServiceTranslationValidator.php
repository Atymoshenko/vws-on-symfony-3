<?php
namespace Control\AdminBundle\Validators;

use Symfony\Component\Validator\Context\ExecutionContextInterface;
use Site\UserBundle\EntityTranslation\PageServiceTranslation;

class PageServiceTranslationValidator
{
	/** @var ExecutionContextInterface */
	private $context;

	public function __construct(ExecutionContextInterface $context)
	{
		$this->context = $context;
	}

	public function validate()
	{
		/** @var PageServiceTranslation $pageServiceTranslation */
		$pageServiceTranslation = $this->context->getObject();

		if ($pageServiceTranslation->getField() == 'name') {
			if (!$pageServiceTranslation->getContent()) {
				//$this->context->buildViolation("Name can't be empty.")->atPath('content')->addViolation();
			}
		}
	}
}