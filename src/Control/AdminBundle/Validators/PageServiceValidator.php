<?php
namespace Control\AdminBundle\Validators;

use Symfony\Component\Validator\Context\ExecutionContextInterface;
use Site\UserBundle\Entity\PageService;
use Site\UserBundle\EntityTranslation\PageServiceTranslation;

class PageServiceValidator
{
	/** @var ExecutionContextInterface */
	private $context;

	public function __construct(ExecutionContextInterface $context)
	{
		$this->context = $context;
	}

	public function validate()
	{
		/** @var PageService $pageService */
		$pageService = $this->context->getObject();

		if (!$pageService->getPath()) {
			$this->context->buildViolation("Path can't be empty.")->atPath('path')->addViolation();
		}

		$translationsFields = [
			'name' => 0,
			'content' => 0,
		];
		/** @var PageServiceTranslation $translation */
		foreach ($pageService->getTranslations() as $translation) {
			if (array_key_exists($translation->getField(), $translationsFields)) {
				$translationsFields[$translation->getField()]++;
			}
		}
		if ($translationsFields['name'] < 1) {
			$this->context->buildViolation("Name can't be empty.")->atPath('translations')->addViolation();
		}
	}
}