<?php
namespace Control\AdminBundle\Validators;

use Symfony\Component\Validator\Context\ExecutionContextInterface;
use Site\UserBundle\Entity\Page;
use Site\UserBundle\EntityTranslation\PageTranslation;

class PageValidator
{
	/** @var ExecutionContextInterface */
	private $context;

	public function __construct(ExecutionContextInterface $context)
	{
		$this->context = $context;
	}

	public function validate()
	{
		/** @var Page $page */
		$page = $this->context->getObject();

		if (!$page->getPath()) {
			$this->context->buildViolation("Path can't be empty.")->atPath('path')->addViolation();
		}

		$translationsFields = [
			'name' => 0,
			'content' => 0,
		];
		/** @var PageTranslation $translation */
		foreach ($page->getTranslations() as $translation) {
			if (array_key_exists($translation->getField(), $translationsFields)) {
				$translationsFields[$translation->getField()]++;
			}
		}
		if ($translationsFields['name'] < 1) {
			$this->context->buildViolation("Name can't be empty.")->atPath('translations')->addViolation();
		}
	}
}