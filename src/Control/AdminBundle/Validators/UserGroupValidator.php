<?php
namespace Control\AdminBundle\Validators;

use Symfony\Component\Validator\Context\ExecutionContextInterface;
use Site\UserBundle\Entity\UserGroup;

class UserGroupValidator
{
	/** @var ExecutionContextInterface */
	private $context;

	public function __construct(ExecutionContextInterface $context)
	{
		$this->context = $context;
	}

	public function validate()
	{
		/** @var UserGroup $userGroup */
		$userGroup = $this->context->getObject();

		if (!$userGroup->getName()) {
			$this->context->buildViolation("Name can't be empty.")->atPath('name')->addViolation();
		}

		if (count($userGroup->getRoles()) < 1) {
			$this->context->buildViolation("Roles can't be empty.")->atPath('roles')->addViolation();
		}
	}
}