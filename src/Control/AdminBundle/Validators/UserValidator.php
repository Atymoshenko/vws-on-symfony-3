<?php
namespace Control\AdminBundle\Validators;

use Symfony\Component\Validator\Context\ExecutionContextInterface;
use Site\UserBundle\Entity\User;

class UserValidator
{
	/** @var ExecutionContextInterface */
	private $context;

	private $payload;

	public function __construct(ExecutionContextInterface $context, $payload)
	{
		$this->context = $context;
		$this->payload = $payload;
	}

	public function validate()
	{
		/** @var User $user */
		$user = $this->context->getObject();

		if (!$user->getEmail()) {
			$this->context->buildViolation("Email can't be empty.")->atPath('email')->addViolation();
		}

		if (!$user->getId() && !$user->getPassword()) {
			$this->context->buildViolation("Password can't be empty.")->atPath('password')->addViolation();
		}

		if ($user->getUserGroups()->count() < 1) {
			$this->context->buildViolation("User groups can't be empty.")->atPath('userGroups')->addViolation();
		}
	}
}