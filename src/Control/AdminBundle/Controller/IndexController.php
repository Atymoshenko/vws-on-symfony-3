<?php
namespace Control\AdminBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;

class IndexController extends Controller
{
    /**
     * @Route("/", name="_admin")
     */
    public function indexAction()
    {
        return $this->render('ControlAdminBundle:Bootstrap/Index:index.html.twig', ['users' => []]);
    }
}
