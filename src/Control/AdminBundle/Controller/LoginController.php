<?php
namespace Control\AdminBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Security\Core\Security;

class LoginController extends Controller
{
	/**
	 * @Route("/login", name="_admin_login")
	 */
	public function loginAction(Request $request)
	{
		$session = $request->getSession();

		// get the login error if there is one
		if ($request->attributes->has(Security::AUTHENTICATION_ERROR)) {
			$error = $request->attributes->get(Security::AUTHENTICATION_ERROR);
		} else {
			$error = $session->get(Security::AUTHENTICATION_ERROR);
			$session->remove(Security::AUTHENTICATION_ERROR);
		}

		$last_username = $session->get(Security::LAST_USERNAME);
		return $this->render('ControlAdminBundle:Bootstrap/Login:login.html.twig', ['error' => $error]);
	}

	/**
	 * @Route("/check_login", name="_admin_check_login")
	 */
	public function checkLoginAction()
	{
		return new Response();
	}

	/**
	 * @Route("/logout", name="_admin_logout")
	 */
	public function logoutAction()
	{
		return $this->redirect($this->generateUrl('_admin_logged_out'));
	}

	/**
	 * @Route("/logged_out", name="_admin_logged_out")
	 */
	public function loggedOutAction()
	{
		//return $this->redirect($this->generateUrl('_admin_login'));
		return $this->render('ControlAdminBundle:Bootstrap/Login:login.html.twig');
	}
}
