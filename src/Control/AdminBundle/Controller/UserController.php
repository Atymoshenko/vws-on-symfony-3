<?php
namespace Control\AdminBundle\Controller;

use Control\AdminBundle\Entity\Filter\UsersListFilter;
use Control\AdminBundle\Form\Filter\UsersListFilterForm;
use Control\AdminBundle\Form\UserForm;
use Control\AdminBundle\Form\UserGroupForm;
use Doctrine\DBAL\Exception\UniqueConstraintViolationException;
use Doctrine\ORM\Tools\Pagination\Paginator;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\Form\FormError;
use Symfony\Component\HttpFoundation\Request;
use Site\UserBundle\Entity\Sex;
use Site\UserBundle\Entity\User;
use Site\CommonBundle\Helpers\PagerHelper;
use Site\UserBundle\Entity\UserGroup;

class UserController extends BaseController
{
	const USERS_PER_PAGE = 30;

	/**
	 * @Route("/users", name="_admin_users")
	 */
	public function usersAction(Request $request)
	{
		$filter = new UsersListFilter();
		$formFilter = $this->createForm(UsersListFilterForm::class, $filter);
		$formFilter->handleRequest($request);
		if ($formFilter->isSubmitted() && $formFilter->isValid() && $filter->hasConditions()) {

		}

		$pager = new PagerHelper($this->getUserRepository()->getFilteredListQuery($filter), self::USERS_PER_PAGE);
		/** @var User[] $users */
		$users = $pager->getItemsByPage($request->get('p'));

		return $this->render('ControlAdminBundle:Bootstrap/User:usersList.html.twig', ['users' => $users, 'pager' => $pager, 'filter' => $filter, 'formFilter' => $formFilter->createView(),]);
	}

	/**
	 * @Route("/user/create", name="_admin_user_create")
	 */
	public function userCreateAction(Request $request)
	{
		$user = new User();

		$form = $this->createForm(UserForm::class, $user, [
			'action' => $this->generateUrl('_admin_user_create'),
			'passwordRequired' => true,
			'em' => $this->em(),
		]);
		$form->handleRequest($request);
		if ($form->isSubmitted() && $form->isValid()) {
			$user->encodePassword($this->container->get('security.password_encoder'));
			try {
				$this->save($user);
				$this->addFlash('success', 'User is created.');

				return $this->redirect($this->generateUrl('_admin_user_edit', ['id' => $user->getId()]));
			} catch (UniqueConstraintViolationException $e) {
				$form->get('email')->addError(new FormError('Use another email. This email already exists.'));
			} catch (\Throwable $e) {
				throw $e;
			}
		}

		return $this->render('ControlAdminBundle:Bootstrap/User:userEdit.html.twig', ['form' => $form->createView(),]);
	}

	/**
	 * @Route("/user/edit/{id}", name="_admin_user_edit")
	 */
	public function userEditAction(Request $request)
	{
		/** @var User $user */
		$user = $this->getUserRepository()->find($request->get('id'));
		if (!$user) {
			throw $this->createNotFoundException('User not found.');
		}
		$userPasswordOld = $user->getPassword();

		$form = $this->createForm(UserForm::class, $user, [
			'action' => $this->generateUrl('_admin_user_edit',['id' => $user->getId()]),
			'em' => $this->em(),
		]);
		$form->handleRequest($request);
		if ($form->isSubmitted() && $form->isValid()) {
			if ($user->getPassword()) {
				$user->encodePassword($this->container->get('security.password_encoder'));
			} else {
				$user->setPassword($userPasswordOld);
			}
			try {
				$this->save($user);
				$this->addFlash('success', 'User is saved.');

				return $this->redirect($this->generateUrl('_admin_user_edit', ['id' => $user->getId()]));
			} catch (UniqueConstraintViolationException $e) {
				$form->get('email')->addError(new FormError('Use another email. This email already exists.'));
			} catch (\Throwable $e) {
				throw $e;
			}
		}

		return $this->render('ControlAdminBundle:Bootstrap/User:userEdit.html.twig', ['form' => $form->createView(),]);
	}

	/**
	 * @Route("/user/remove/{id}", name="_admin_user_remove")
	 */
	public function userRemoveAction(Request $request)
	{
		$user = $this->getUserRepository()->find($request->get('id'));
		if (!$user) {
			throw $this->createNotFoundException('User not found.');
		}

		$user->setDeleted(true);
		$this->save($user);
		$this->addFlash('success', 'User is removed.');

		return $this->redirect($this->generateUrl('_admin_users'));
	}

	/**
	 * @Route("/usergroups", name="_admin_usergroups")
	 */
	public function userGroupsAction(Request $request)
	{
		$pager = new PagerHelper($this->getUserGroupRepository()->getFilteredListQuery());
		$userGroups = $pager->getItemsByPage($request->get('p'));

		return $this->render('ControlAdminBundle:Bootstrap/User:userGroupsList.html.twig', ['userGroups' => $userGroups, 'pager' => $pager]);
	}

	/**
	 * @Route("/usergroup/create", name="_admin_usergroup_create")
	 */
	public function userGroupCreateAction(Request $request)
	{
		$userGroup = new UserGroup();

		$form = $this->createForm(UserGroupForm::class, $userGroup, ['action' => $this->generateUrl('_admin_usergroup_create')]);
		$form->handleRequest($request);
		if ($form->isSubmitted() && $form->isValid()) {
			$this->save($userGroup);
			$this->addFlash('success', 'User group is created.');

			return $this->redirect($this->generateUrl('_admin_usergroup_edit', ['id' => $userGroup->getId()]));
		}

		return $this->render('ControlAdminBundle:Bootstrap/User:userGroupEdit.html.twig', ['form' => $form->createView(),]);
	}

	/**
	 * @Route("/usergroup/edit/{id}", name="_admin_usergroup_edit")
	 */
	public function userGroupEditAction(Request $request)
	{
		/** @var UserGroup $userGroup */
		$userGroup = $this->getUserGroupRepository()->find($request->get('id'));
		if (!$userGroup) {
			throw $this->createNotFoundException('User group not found.');
		}

		$form = $this->createForm(UserGroupForm::class, $userGroup, ['action' => $this->generateUrl('_admin_usergroup_edit', ['id' => $userGroup->getId()]),]);
		$form->handleRequest($request);
		if ($form->isSubmitted() && $form->isValid()) {
			$this->save($userGroup);
			$this->addFlash('success', 'User group is saved.');

			return $this->redirect($this->generateUrl('_admin_usergroup_edit', ['id' => $userGroup->getId()]));
		}

		return $this->render('ControlAdminBundle:Bootstrap/User:userGroupEdit.html.twig', ['form' => $form->createView(),]);
	}

	/**
	 * @Route("/usergroup/delete/{id}", name="_admin_usergroup_delete")
	 */
	public function userGroupRemoveAction(Request $request)
	{
		$userGroup = $this->getUserGroupRepository()->find($request->get('id'));
		if (!$userGroup) {
			throw $this->createNotFoundException('User group not found.');
		}

		$this->remove($userGroup);
		$this->addFlash('success', 'User group is removed.');

		return $this->redirect($this->generateUrl('_admin_usergroups'));
	}

}
