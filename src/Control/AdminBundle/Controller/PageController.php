<?php
namespace Control\AdminBundle\Controller;

use Control\AdminBundle\Entity\Filter\PagesListFilter;
use Control\AdminBundle\Entity\PageHistory;
use Control\AdminBundle\Form\Filter\PagesListFilterForm;
use Control\AdminBundle\Form\PageForm;
use Doctrine\DBAL\Exception\UniqueConstraintViolationException;
use Doctrine\ORM\Tools\Pagination\Paginator;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\Form\FormError;
use Symfony\Component\HttpFoundation\Request;
use Site\UserBundle\Entity\Locale;
use Site\UserBundle\Entity\Page;
use Site\CommonBundle\Helpers\PagerHelper;

class PageController extends BaseController
{
	/**
	 * @Route("/pages", name="_admin_pages")
	 */
	public function pagesAction(Request $request)
	{
		$filter = new PagesListFilter();
		$formFilter = $this->createForm(PagesListFilterForm::class, $filter);
		$formFilter->handleRequest($request);
		if ($formFilter->isSubmitted() && $formFilter->isValid() && $filter->hasConditions()) {

		}

		$pager = new PagerHelper($this->getPageRepository()->getFilteredListQuery($filter));
		/** @var Page[] $pages */
		$pages = $pager->getItemsByPage($request->get('p'));

		return $this->render('ControlAdminBundle:Bootstrap/Page:pagesList.html.twig', ['pages' => $pages, 'pager' => $pager, 'filter' => $filter, 'formFilter' => $formFilter->createView(),]);
	}

	/**
	 * @Route("/page/create", name="_admin_page_create")
	 */
	public function pageCreateAction(Request $request)
	{
		$page = new Page();
		$page->fillTranslations(Locale::getAll());

		$form = $this->createForm(PageForm::class, $page, [
			'action' => $this->generateUrl('_admin_page_create'),
		]);
		$form->handleRequest($request);
		if ($form->isSubmitted() && $form->isValid()) {
			try {
				$page->setCreatedUser($this->getUser());
				$this->save($page);
				$this->addFlash('success', 'Page is created.');

				return $this->redirect($this->generateUrl('_admin_page_edit', ['id' => $page->getId()]));
			} catch (UniqueConstraintViolationException $e) {
				$form->get('path')->addError(new FormError('Use another path. This path already exists.'));
			} catch (\Throwable $e) {
				throw $e;
			}
		}

		return $this->render('ControlAdminBundle:Bootstrap/Page:pageEdit.html.twig', ['form' => $form->createView(),]);
	}

	/**
	 * @Route("/page/edit/{id}", name="_admin_page_edit")
	 */
	public function pageEditAction(Request $request)
	{
		/** @var Page $page */
		$page = $this->getPageRepository()->find($request->get('id'));
		if (!$page) {
			throw $this->createNotFoundException('Page not found.');
		}

		$page->fillTranslations(Locale::getAll());
		$pageHistory = new PageHistory($page, $this->getUser());
		$form = $this->createForm(PageForm::class, $page, [
			'action' => $this->generateUrl('_admin_page_edit',['id' => $page->getId()]),
		]);
		$form->handleRequest($request);
		if ($form->isSubmitted() && $form->isValid()) {
			try {
				if ($pageHistory->isDifferent($page)) {
					$this->save($page);
					$this->save($pageHistory);
				}
				$this->addFlash('success', 'Page is saved.');

				return $this->redirect($this->generateUrl('_admin_page_edit', ['id' => $page->getId()]));
			} catch (UniqueConstraintViolationException $e) {
				$form->get('path')->addError(new FormError('Use another path. This path already exists.'));
			} catch (\Throwable $e) {
				throw $e;
			}
		}

		return $this->render('ControlAdminBundle:Bootstrap/Page:pageEdit.html.twig', ['form' => $form->createView(),]);
	}

	/**
	 * @Route("/page/delete/{id}", name="_admin_page_delete")
	 */
	public function pageDeleteAction(Request $request)
	{
		$page = $this->getPageRepository()->find($request->get('id'));
		if (!$page) {
			throw $this->createNotFoundException('Page not found.');
		}

		$this->remove($page);
		$this->addFlash('success', 'Page is removed.');

		return $this->redirect($this->generateUrl('_admin_pages'));
	}
}
