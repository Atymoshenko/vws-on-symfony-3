<?php
namespace Control\AdminBundle\Controller;

use Site\CommonBundle\Traits\RepositoryTrait;
use Doctrine\ORM\EntityManager;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class BaseController extends Controller
{
	use RepositoryTrait;

	/** @var EntityManager */
	private $em;

	protected function em(): EntityManager
	{
		if (!$this->em) {
			$this->em = $this->getDoctrine()->getManager();
		}

		return $this->em;
	}
}