<?php
namespace Control\AdminBundle\Controller;

use Control\AdminBundle\Entity\PageServiceHistory;
use Control\AdminBundle\Form\PageServiceForm;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;
use Site\UserBundle\Entity\Locale;
use Site\UserBundle\Entity\PageService;

class PageServiceController extends BaseController
{
	/**
	 * @Route("/pages_service", name="_admin_pages_service")
	 */
	public function pagesServiceAction(Request $request)
	{
		$pagesService = $this->getPageServiceRepository()->findAll();
		/*dump($this->container->hasParameter('pages_service'));
		exit;
		$pagesServiceAllowed = $this->container->getParameter('pages_service');
		dump($pagesServiceAllowed);exit;*/

		return $this->render('ControlAdminBundle:Bootstrap/PageService:pagesServiceList.html.twig', ['pagesService' => $pagesService]);
	}

	/**
	 * @Route("/page/service/edit/{id}", name="_admin_page_service_edit")
	 */
	public function pageServiceEditAction(Request $request)
	{
		/** @var PageService $pageService */
		$pageService = $this->getPageServiceRepository()->find($request->get('id'));
		if (!$pageService) {
			throw $this->createNotFoundException('Page service not found.');
		}

		$pageService->fillTranslations(Locale::getAll());
		$pageServiceHistory = new PageServiceHistory($pageService, $this->getUser());
		$form = $this->createForm(PageServiceForm::class, $pageService, [
			'action' => $this->generateUrl('_admin_page_service_edit',['id' => $pageService->getId()]),
		]);
		$form->handleRequest($request);
		if ($form->isSubmitted() && $form->isValid()) {
			try {
				if ($pageServiceHistory->isDifferent($pageService)) {
					$this->save($pageService);
					$this->save($pageServiceHistory);
				}
				$this->addFlash('success', 'Page service is saved.');

				return $this->redirect($this->generateUrl('_admin_page_service_edit', ['id' => $pageService->getId()]));
			}
			catch (\Throwable $e) {
				throw $e;
			}
		}

		return $this->render('ControlAdminBundle:Bootstrap/PageService:pageServiceEdit.html.twig', ['form' => $form->createView(),]);
	}
}
