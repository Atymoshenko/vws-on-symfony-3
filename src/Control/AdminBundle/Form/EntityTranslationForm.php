<?php
namespace Control\AdminBundle\Form;

use Site\CommonBundle\Traits\RepositoryTrait;
use Doctrine\ORM\EntityManager;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\OptionsResolver\Options;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Gedmo\Translatable\Entity\MappedSuperclass\AbstractPersonalTranslation;

class EntityTranslationForm extends AbstractType
{
	use RepositoryTrait;

	/** @var FormBuilderInterface */
	private $builder;

	/** @var EntityManager */
	private $em;

	/** @var array */
	private $fieldMap;

	protected function em(): EntityManager
	{
		return $this->em;
	}

	public function buildForm(FormBuilderInterface $builder, array $options)
    {
    	$this->builder = $builder;
    	$this->fieldMap = $options['fieldMap'];

		$this->buildContent();
    }

	private function buildContent()
	{
		//$this->builder->add($this->fieldName);
		$this->builder->addEventListener(FormEvents::PRE_SET_DATA,
			function (FormEvent $event)
			{
				$form = $event->getForm();
				/** @var AbstractPersonalTranslation $entity */
				$entity = $event->getData();
				if ($entity instanceof AbstractPersonalTranslation && array_key_exists($entity->getField(), $this->fieldMap)) {
					$form->add('content', $this->fieldMap[$entity->getField()]['type'], [
						'label' => $this->fieldMap[$entity->getField()]['label'] . ' (' . $entity->getLocale() . ')',
						'label_format' => 'form.content.%name%',
						'required' => $this->fieldMap[$entity->getField()]['required'],
						'attr' => [
							'data-locale' => $entity->getLocale(),
							'data-field' => $entity->getField(),
						],
					]);
				}
			}
		);
	}

	public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
			'data_class' => function (Options $options) {
				return $options->offsetGet('data_class');
			},
			'fieldMap' => function (Options $options) {
				return $options->offsetGet('fieldMap');
			},
        ));

        $resolver->setRequired(['data_class','fieldMap']);
    }

    public function getBlockPrefix(): string
    {
        return 'entity_translation';
    }
}