<?php
namespace Control\AdminBundle\Form;

use Site\CommonBundle\Traits\RepositoryTrait;
use Doctrine\ORM\EntityManager;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\Valid;
use Site\UserBundle\Entity\Locale;
use Site\UserBundle\Entity\PageService;
use Site\UserBundle\EntityTranslation\PageServiceTranslation;

class PageServiceForm extends AbstractType
{
	use RepositoryTrait;

	/** @var FormBuilderInterface */
	private $builder;

	/** @var EntityManager */
	private $em;

	protected function em(): EntityManager
	{
		return $this->em;
	}

	public function buildForm(FormBuilderInterface $builder, array $options)
    {
    	//$this->em = $options['em'];

    	$this->builder = $builder;
    	$this->builder->setMethod('POST');

		$this->buildLocale();
        $this->buildTranslations();

        $this->builder->add('save', SubmitType::class);
    }

    private function buildLocale()
	{
		$this->builder->add('locale', ChoiceType::class, [
			'label' => 'Locale',
			'choices' => Locale::getAllForForm(),
			'mapped' => false,
			'attr' => ['class' => 'field-locale'],
		]);
	}

	private function buildTranslations()
	{
		$this->builder->add('translations', CollectionType::class, [
			'label' => false,
			'constraints' => new Valid(),
			'entry_type' => EntityTranslationForm::class,
			'entry_options' => [
				'label' => false,
				'data_class' => 'Site\UserBundle\EntityTranslation\PageServiceTranslation',
				'fieldMap' => [
					'name' => [
						'label' => 'Name',
						'type' => TextType::class,
						'required' => false,
					],
					'content' => [
						'label' => 'Content',
						'type' => TextareaType::class,
						'required' => false,
					],
					'metaTitle' => [
						'label' => 'Meta title',
						'type' => TextType::class,
						'required' => false,
					],
					'metaKeywords' => [
						'label' => 'Meta keywords',
						'type' => TextType::class,
						'required' => false,
					],
					'metaDescription' => [
						'label' => 'Meta description',
						'type' => TextType::class,
						'required' => false,
					],
				],
			],
		]);
	}

	public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Site\UserBundle\Entity\PageService',
			'csrf_protection' => false,
        ));

        //$resolver->setRequired('em');
    }

    public function getBlockPrefix(): string
    {
        return 'page_service';
    }
}