<?php
namespace Control\AdminBundle\Form;

use Site\CommonBundle\Traits\RepositoryTrait;
use Doctrine\ORM\EntityManager;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Site\UserBundle\Entity\Sex;
use Site\UserBundle\Entity\UserGroup;

class UserForm extends AbstractType
{
	use RepositoryTrait;

	/** @var FormBuilderInterface */
	private $builder;

	/** @var EntityManager */
	private $em;

	protected function em(): EntityManager
	{
		return $this->em;
	}

	public function buildForm(FormBuilderInterface $builder, array $options)
    {
    	$this->em = $options['em'];

    	$this->builder = $builder;
    	$this->builder->setMethod('POST');

		$this->buildGroup();
        $this->buildEmail();
		$this->buildSex();
        $this->buildPassword(['required' => $options['passwordRequired']]);
		$this->buildDescription();
		$this->buildIsRemoved();

        $this->builder->add('save', SubmitType::class);
    }

	private function buildGroup()
	{
		$this->builder->add('userGroups', EntityType::class, [
			'label' => 'Groups',
			'class' => 'Site\UserBundle\Entity\UserGroup',
			'choice_label' => function(UserGroup $userGroup) {
				return $userGroup->getName();
			},
			'multiple' => true,
		]);
	}

	private function buildEmail()
	{
		$this->builder->add('email', EmailType::class, [
			'label' => 'Email',
		]);
	}

	private function buildSex()
	{
		$this->builder->add('sex', ChoiceType::class, [
			'label' => 'Sex',
			'placeholder' => 'Not set',
			'required' => false,
			'choice_value' => function(?Sex $sex) {
				return $sex ? $sex->getCode() : null;
			},
			'choice_label' => function(Sex $sex) {
				return $sex->getValue();
			},
			'choices' => Sex::getAllForFormAsEntities(),
		]);
	}

	private function buildPassword(array $options = [])
	{
		$this->builder->add('password', PasswordType::class, array_merge([
			'label' => 'Password',
			'required' => false,
		], $options));
	}

	private function buildIsRemoved()
	{
		$this->builder->add('removed', CheckboxType::class, [
			'label' => 'Removed',
			'required' => false,
		]);
	}

	private function buildDescription()
	{
		$this->builder->add('description', TextareaType::class, [
			'label' => 'Description',
			'required' => false,
		]);
	}

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Site\UserBundle\Entity\User',
			'csrf_protection' => false,
			'passwordRequired' => false,
        ));

        $resolver->setRequired('em');
    }

    public function getBlockPrefix(): string
    {
        return 'user';
    }
}
