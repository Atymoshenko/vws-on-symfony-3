<?php
namespace Control\AdminBundle\Form;

use Site\CommonBundle\Entity\Role;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class UserGroupForm extends AbstractType
{
	/** @var FormBuilderInterface */
	private $builder;

	public function buildForm(FormBuilderInterface $builder, array $options)
    {
    	$this->builder = $builder;
    	$this->builder->setMethod('POST');

        $this->buildName();
		$this->buildRoles();

        $this->builder->add('save', SubmitType::class);
    }

    private function buildName()
	{
		$this->builder->add('name', TextType::class, [
			'label' => 'Name',
			'attr' => ['placeholder' => 'Name'],
		]);
	}

	private function buildRoles()
	{
		$this->builder->add('roles', ChoiceType::class, [
			'label' => 'Roles',
			'choices' => Role::getAllForForm(),
			'multiple' => true,
		]);
	}

	public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Site\UserBundle\Entity\UserGroup',
			'csrf_protection' => false,
        ));
    }

    public function getBlockPrefix(): string
    {
        return 'user_group';
    }
}
