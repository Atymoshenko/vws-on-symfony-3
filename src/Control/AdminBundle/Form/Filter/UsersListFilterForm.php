<?php

namespace Control\AdminBundle\Form\Filter;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\ResetType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Site\UserBundle\Entity\Sex;

class UsersListFilterForm extends AbstractType
{
	/** @var FormBuilderInterface */
	private $builder;

	public function buildForm(FormBuilderInterface $builder, array $options)
    {
    	$this->builder = $builder;
    	$this->builder->setMethod('GET');

        $this->buildEmail();
		$this->buildSex();
		$this->buildWithRemoved();
    }

    private function buildEmail()
	{
		$this->builder->add('email', TextType::class, [
			'label' => 'Email',
			'required' => false,
		]);
	}

	private function buildSex()
	{
		$this->builder->add('sex', ChoiceType::class, [
			'label' => 'Sex',
			'choices' => array_flip(Sex::getAllForForm()),
			'required' => false,
		]);
	}

	private function buildWithRemoved()
	{
		$this->builder->add('withRemoved', CheckboxType::class, [
			'label' => 'With removed',
			'required' => false,
		]);
	}

	public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Control\AdminBundle\Entity\Filter\UsersListFilter',
			'csrf_protection' => false,
        ));
    }

    public function getBlockPrefix(): string
    {
        return '';
    }
}
