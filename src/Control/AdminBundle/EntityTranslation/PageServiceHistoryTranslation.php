<?php
namespace Control\AdminBundle\EntityTranslation;

use Doctrine\ORM\Mapping as ORM;
use Gedmo\Translatable\Entity\MappedSuperclass\AbstractPersonalTranslation;

/**
 * @ORM\Entity
 * @ORM\Table(name="page_service_history_translation",
 *     uniqueConstraints={@ORM\UniqueConstraint(name="objectId_locale_field", columns={
 *         "objectId", "locale", "field"
 *     })}
 * )
 */
class PageServiceHistoryTranslation extends AbstractPersonalTranslation
{
	/**
	 * Convenient constructor
	 *
	 * @param string $locale
	 * @param string $field
	 * @param string $value
	 */
	public function __construct($locale, $field, $value)
	{
		$this->setLocale($locale);
		$this->setField($field);
		$this->setContent($value);
	}

	/**
	 * @ORM\ManyToOne(targetEntity="Control\AdminBundle\Entity\PageServiceHistory", inversedBy="translations")
	 * @ORM\JoinColumn(name="objectId", referencedColumnName="id", onDelete="CASCADE")
	 */
	protected $object;
}