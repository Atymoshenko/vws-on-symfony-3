<?php
namespace Control\AdminBundle\Entity\Filter;

use Site\CommonBundle\Entity\Filter\ListFilter;
use Site\UserBundle\Entity\Sex;

class UsersListFilter extends ListFilter
{
	/** @var string|null */
	protected $email;

	/** @var Sex */
	protected $sex;

	/** @var bool|null */
	protected $withRemoved;

	public function getEmail(): ?string
	{
		return $this->email;
	}

	public function setEmail(?string $email)
	{
		$this->email = $email;
	}

	public function getSex(): ?string
	{
		return $this->sex;
	}

	public function setSex(?string $sex)
	{
		$this->sex = $sex;
	}

	public function getWithRemoved(): ?bool
	{
		return $this->withRemoved;
	}

	public function setWithRemoved(bool $withRemoved)
	{
		$this->withRemoved = $withRemoved;
		if (!$this->withRemoved) {
			$this->withRemoved = null;
		}
	}

}