<?php
namespace Control\AdminBundle\Entity\Filter;

use Site\CommonBundle\Entity\Filter\ListFilter;

class PagesListFilter extends ListFilter
{
	/** @var string|null */
	protected $name;

	public function getName(): ?string
	{
		return $this->name;
	}

	public function setName(?string $name)
	{
		$this->name = $name;
	}

}