<?php
namespace Control\AdminBundle\Entity;

use Control\AdminBundle\EntityTranslation\PageHistoryTranslation;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Site\UserBundle\Entity\Page;
use Site\UserBundle\Entity\User;
use Site\UserBundle\EntityTranslation\PageTranslation;

/**
 * @ORM\Entity(repositoryClass="Control\AdminBundle\Repository\PageHistoryRepository")
 * @ORM\Table(name="page_history")
 * @ORM\HasLifecycleCallbacks()
 * @Gedmo\TranslationEntity(class="Control\AdminBundle\EntityTranslation\PageHistoryTranslation")
 */
class PageHistory
{
	/**
	 * @var int
	 * @ORM\Column(name="id", type="integer")
	 * @ORM\Id
	 * @ORM\GeneratedValue(strategy="AUTO")
	 */
	private $id;

	/**
	 * @var Page
	 * @ORM\ManyToOne(targetEntity="Site\UserBundle\Entity\Page")
	 * @ORM\JoinColumn(name="pageId", referencedColumnName="id")
	 */
	private $page;

	/**
	 * @var PageHistory
	 * @ORM\ManyToOne(targetEntity="PageHistory")
	 * @ORM\JoinColumn(name="fromPageHistoryId", referencedColumnName="id")
	 */
	private $fromPageHistory;

	/**
	 * @var string
	 * @ORM\Column(name="path", type="string", length=255)
	 */
	private $path;

	/**
	 * @var bool
	 * @ORM\Column(name="active", type="boolean", nullable=false)
	 */
	private $active = false;

	/**
	 * @var bool
	 * @ORM\Column(name="visible", type="boolean", nullable=false)
	 */
	private $visible = true;

	/**
	 * @var string
	 * @ORM\Column(name="name", type="string", length=255)
	 * @Gedmo\Translatable
	 */
	private $name;

	/**
	 * @var string
	 * @ORM\Column(name="content", type="text")
	 * @Gedmo\Translatable
	 */
	private $content;

	/**
	 * @var string
	 * @ORM\Column(name="metaTitle", type="string", length=255)
	 * @Gedmo\Translatable
	 */
	private $metaTitle;

	/**
	 * @var string
	 * @ORM\Column(name="metaKeywords", type="string", length=255)
	 * @Gedmo\Translatable
	 */
	private $metaKeywords;

	/**
	 * @var string
	 * @ORM\Column(name="metaDescription", type="string", length=255)
	 * @Gedmo\Translatable
	 */
	private $metaDescription;

	/**
	 * @var User
	 * @ORM\ManyToOne(targetEntity="Site\UserBundle\Entity\User")
	 * @ORM\JoinColumn(name="modifiedUserId", referencedColumnName="id")
	 */
	private $modifiedUser;

	/**
	 * @var \DateTime
	 * @ORM\Column(name="dateModified", type="datetime", nullable=false)
	 */
	private $dateModified;

	/**
	 * @Gedmo\Locale
	 * Used locale to override Translation listener`s locale
	 * this is not a mapped field of entity metadata, just a simple property
	 */
	protected $locale;

	/**
	 * @ORM\OneToMany(targetEntity="Control\AdminBundle\EntityTranslation\PageHistoryTranslation", mappedBy="object",
	 *   cascade={"persist", "remove"}
	 * )
	 */
	private $translations;

	public function __construct(Page $page, User $user)
	{
		$this->page = $page;
		$this->modifiedUser = $user;

		$this->path = $page->getPath();
		$this->active = $page->isActive();
		$this->visible = $page->isVisible();

		$this->translations = new ArrayCollection();

		/** @var PageTranslation $pageTranslation */
		foreach ($page->getTranslations() as $pageTranslation) {
			$pageHistoryTranslation = new PageHistoryTranslation(
				$pageTranslation->getLocale(),
				$pageTranslation->getField(),
				$pageTranslation->getContent()
			);

			$this->addTranslation($pageHistoryTranslation);
		}
	}

	public function getTranslations()
	{
		return $this->translations;
	}

	public function addTranslation(PageHistoryTranslation $t)
	{
		if (!$this->translations->contains($t)) {
			$this->translations[] = $t;
			$t->setObject($this);
		}
	}

	public function setLocale(string $locale)
	{
		$this->locale = $locale;
	}

	public function getId(): int
	{
		return $this->id;
	}

	public function getPage(): Page
	{
		return $this->page;
	}

	public function getFromPageHistory(): ?PageHistory
	{
		return $this->fromPageHistory;
	}

	public function setFromPageHistory(?PageHistory $fromPageHistory)
	{
		$this->fromPageHistory = $fromPageHistory;
	}

	public function setPath(string $path)
	{
		$this->path = $path;
	}

	public function getPath(): ?string
	{
		return $this->path;
	}

	public function isActive(): bool
	{
		return $this->active;
	}

	public function setActive(bool $active)
	{
		$this->active = $active;
	}

	public function isVisible(): bool
	{
		return $this->visible;
	}

	public function setVisible(bool $visible)
	{
		$this->visible = $visible;
	}

	public function getName(): ?string
	{
		return $this->name;
	}

	public function setName(?string $name)
	{
		$this->name = $name;
	}

	public function getContent(): ?string
	{
		return $this->content;
	}

	public function setContent(?string $content)
	{
		$this->content = $content;
	}

	public function getMetaTitle(): ?string
	{
		return $this->metaTitle;
	}

	public function setMetaTitle(?string $metaTitle)
	{
		$this->metaTitle = $metaTitle;
	}

	public function getMetaKeywords(): ?string
	{
		return $this->metaKeywords;
	}

	public function setMetaKeywords(?string $metaKeywords)
	{
		$this->metaKeywords = $metaKeywords;
	}

	public function getMetaDescription(): ?string
	{
		return $this->metaDescription;
	}

	public function setMetaDescription(?string $metaDescription)
	{
		$this->metaDescription = $metaDescription;
	}

	public function getModifiedUser(): User
	{
		return $this->modifiedUser;
	}

	public function getDateModified(): \DateTime
	{
		return $this->dateModified;
	}

	public function isDifferent(Page $page): bool
	{
		if ($page->getPath() !== $this->getPath()) {
			return true;
		}
		if ($page->isActive() !== $this->isActive()) {
			return true;
		}
		if ($page->isVisible() !== $this->isVisible()) {
			return true;
		}
		if ($page->getTranslations()->count() != $this->getTranslations()->count()) {
			return true;
		}
		/** @var PageTranslation $pageTranslation */
		foreach ($page->getTranslations() as $pageTranslation) {
			/** @var PageHistoryTranslation $pageHistoryTranslation */
			foreach ($this->getTranslations() as $pageHistoryTranslation) {
				if ($pageTranslation->getField() !== $pageHistoryTranslation->getField()
					|| $pageTranslation->getLocale() !== $pageHistoryTranslation->getLocale()) {
					continue;
				}
				if ($pageTranslation->getContent() != $pageHistoryTranslation->getContent()) {
					return true;
				}
			}
		}

		return false;
	}

	/**
	 * @ORM\PreFlush
	 */
	public function doOnPreFlush()
	{
		$this->dateModified = new \DateTime('now');
	}
}