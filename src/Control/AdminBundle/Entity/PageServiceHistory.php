<?php
namespace Control\AdminBundle\Entity;

use Control\AdminBundle\EntityTranslation\PageServiceHistoryTranslation;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Site\UserBundle\Entity\PageService;
use Site\UserBundle\Entity\User;
use Site\UserBundle\EntityTranslation\PageServiceTranslation;

/**
 * @ORM\Entity(repositoryClass="Control\AdminBundle\Repository\PageServiceHistoryRepository")
 * @ORM\Table(name="page_service_history")
 * @ORM\HasLifecycleCallbacks()
 * @Gedmo\TranslationEntity(class="Control\AdminBundle\EntityTranslation\PageServiceHistoryTranslation")
 */
class PageServiceHistory
{
	/**
	 * @var int
	 * @ORM\Column(name="id", type="integer")
	 * @ORM\Id
	 * @ORM\GeneratedValue(strategy="AUTO")
	 */
	private $id;

	/**
	 * @var PageService
	 * @ORM\ManyToOne(targetEntity="Site\UserBundle\Entity\PageService")
	 * @ORM\JoinColumn(name="pageServiceId", referencedColumnName="id")
	 */
	private $pageService;

	/**
	 * @var PageServiceHistory
	 * @ORM\ManyToOne(targetEntity="PageServiceHistory")
	 * @ORM\JoinColumn(name="fromPageServiceHistoryId", referencedColumnName="id")
	 */
	private $fromPageServiceHistory;

	/**
	 * @var string
	 * @ORM\Column(name="path", type="string", length=255)
	 */
	private $path;

	/**
	 * @var string
	 * @ORM\Column(name="name", type="string", length=255)
	 * @Gedmo\Translatable
	 */
	private $name;

	/**
	 * @var string
	 * @ORM\Column(name="content", type="text")
	 * @Gedmo\Translatable
	 */
	private $content;

	/**
	 * @var string
	 * @ORM\Column(name="metaTitle", type="string", length=255)
	 * @Gedmo\Translatable
	 */
	private $metaTitle;

	/**
	 * @var string
	 * @ORM\Column(name="metaKeywords", type="string", length=255)
	 * @Gedmo\Translatable
	 */
	private $metaKeywords;

	/**
	 * @var string
	 * @ORM\Column(name="metaDescription", type="string", length=255)
	 * @Gedmo\Translatable
	 */
	private $metaDescription;

	/**
	 * @var User
	 * @ORM\ManyToOne(targetEntity="Site\UserBundle\Entity\User")
	 * @ORM\JoinColumn(name="modifiedUserId", referencedColumnName="id")
	 */
	private $modifiedUser;

	/**
	 * @var \DateTime
	 * @ORM\Column(name="dateModified", type="datetime", nullable=false)
	 */
	private $dateModified;

	/**
	 * @Gedmo\Locale
	 * Used locale to override Translation listener`s locale
	 * this is not a mapped field of entity metadata, just a simple property
	 */
	protected $locale;

	/**
	 * @ORM\OneToMany(targetEntity="Control\AdminBundle\EntityTranslation\PageServiceHistoryTranslation", mappedBy="object",
	 *   cascade={"persist", "remove"}
	 * )
	 */
	private $translations;

	public function __construct(PageService $pageService, User $user)
	{
		$this->pageService = $pageService;
		$this->modifiedUser = $user;

		$this->path = $pageService->getPath();

		$this->translations = new ArrayCollection();

		/** @var PageServiceTranslation $translation */
		foreach ($pageService->getTranslations() as $translation) {
			$pageServiceHistoryTranslation = new PageServiceHistoryTranslation(
				$translation->getLocale(),
				$translation->getField(),
				$translation->getContent()
			);

			$this->addTranslation($pageServiceHistoryTranslation);
		}
	}

	public function getTranslations()
	{
		return $this->translations;
	}

	public function addTranslation(PageServiceHistoryTranslation $t)
	{
		if (!$this->translations->contains($t)) {
			$this->translations[] = $t;
			$t->setObject($this);
		}
	}

	public function setLocale(string $locale)
	{
		$this->locale = $locale;
	}

	public function getId(): int
	{
		return $this->id;
	}

	public function getPageService(): PageService
	{
		return $this->pageService;
	}

	public function getFromPageServiceHistory(): ?PageServiceHistory
	{
		return $this->fromPageServiceHistory;
	}

	public function setFromPageServiceHistory(?PageServiceHistory $fromPageServiceHistory)
	{
		$this->fromPageServiceHistory = $fromPageServiceHistory;
	}

	public function setPath(string $path)
	{
		$this->path = $path;
	}

	public function getPath(): ?string
	{
		return $this->path;
	}

	public function getName(): ?string
	{
		return $this->name;
	}

	public function setName(?string $name)
	{
		$this->name = $name;
	}

	public function getContent(): ?string
	{
		return $this->content;
	}

	public function setContent(?string $content)
	{
		$this->content = $content;
	}

	public function getMetaTitle(): ?string
	{
		return $this->metaTitle;
	}

	public function setMetaTitle(?string $metaTitle)
	{
		$this->metaTitle = $metaTitle;
	}

	public function getMetaKeywords(): ?string
	{
		return $this->metaKeywords;
	}

	public function setMetaKeywords(?string $metaKeywords)
	{
		$this->metaKeywords = $metaKeywords;
	}

	public function getMetaDescription(): ?string
	{
		return $this->metaDescription;
	}

	public function setMetaDescription(?string $metaDescription)
	{
		$this->metaDescription = $metaDescription;
	}

	public function getModifiedUser(): User
	{
		return $this->modifiedUser;
	}

	public function getDateModified(): \DateTime
	{
		return $this->dateModified;
	}

	public function isDifferent(PageService $pageService): bool
	{
		if ($pageService->getPath() !== $this->getPath()) {
			return true;
		}
		if ($pageService->getTranslations()->count() != $this->getTranslations()->count()) {
			return true;
		}
		/** @var PageServiceTranslation $pageServiceTranslation */
		foreach ($pageService->getTranslations() as $pageServiceTranslation) {
			/** @var PageServiceHistoryTranslation $pageServiceHistoryTranslation */
			foreach ($this->getTranslations() as $pageServiceHistoryTranslation) {
				if ($pageServiceTranslation->getField() !== $pageServiceHistoryTranslation->getField()
					|| $pageServiceTranslation->getLocale() !== $pageServiceHistoryTranslation->getLocale()) {
					continue;
				}
				if ($pageServiceTranslation->getContent() != $pageServiceHistoryTranslation->getContent()) {
					return true;
				}
			}
		}

		return false;
	}

	/**
	 * @ORM\PreFlush
	 */
	public function doOnPreFlush()
	{
		$this->dateModified = new \DateTime('now');
	}
}