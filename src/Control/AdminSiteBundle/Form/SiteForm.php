<?php
namespace Control\AdminSiteBundle\Form;

use Site\CommonBundle\Traits\RepositoryTrait;
use Doctrine\ORM\EntityManager;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\DateTimeType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Site\UserBundle\Entity\Sex;
use Site\UserBundle\Entity\UserGroup;
use Symfony\Component\Validator\Constraints\Valid;
use Site\MultiSiteBundle\Entity\Environment;
use Site\MultiSiteBundle\Entity\Site;

class SiteForm extends AbstractType
{
	use RepositoryTrait;

	/** @var FormBuilderInterface */
	private $builder;

	/** @var EntityManager */
	private $em;

	protected function em(): EntityManager
	{
		return $this->em;
	}

	public function buildForm(FormBuilderInterface $builder, array $options)
    {
    	//$this->em = $options['em'];

    	$this->builder = $builder;
    	$this->builder->setMethod('POST');

		$this->buildUserId();
		$this->buildEnvironment();
		$this->buildDateExpire();
		$this->buildDatabaseParameters();
		$this->buildDomains();

        $this->builder->add('save', SubmitType::class);
    }

	private function buildUserId()
	{
		$this->builder->add('userId');
		$this->builder->addEventListener(FormEvents::PRE_SET_DATA,
			function (FormEvent $event)
			{
				$form = $event->getForm();
				/** @var Site $entity */
				$entity = $event->getData();
				$form->add('userId', IntegerType::class, [
					'label' => 'User id',
					'required' => false,
					'data' => $entity->getUser() ? $entity->getUser()->getId() : null,
				]);
			}
		);
	}

	private function buildEnvironment()
	{
		$this->builder->add('environment', ChoiceType::class, [
			'label' => 'Environment',
			'placeholder' => 'Not set',
			'choice_value' => function(?Environment $environment) {
				return $environment ? $environment->getCode() : null;
			},
			'choice_label' => function(Environment $environment) {
				return $environment->getValue();
			},
			'choices' => Environment::getAllForFormAsEntities(),
		]);
	}

	private function buildDateExpire()
	{
		$this->builder->add('dateExpire', DateTimeType::class, [
			'label' => 'Expire date and time',
			'widget' => 'single_text',
			'format' => 'yyyy-MM-dd HH:mm',
		]);
	}

	private function buildDatabaseParameters()
	{
		$this->builder->add('databaseHost', TextType::class, [
			'label' => 'Database host',
			'required' => false,
		]);
		$this->builder->add('databaseName', TextType::class, [
			'label' => 'Database name',
			'required' => false,
		]);
		$this->builder->add('databaseUser', TextType::class, [
			'label' => 'Database user',
			'required' => false,
		]);
		$this->builder->add('databasePassword', TextType::class, [
			'label' => 'Database password',
			'required' => false,
		]);
	}

	private function buildDomains()
	{
		$this->builder->add('domains', CollectionType::class, [
			'label' => 'Domains',
			'constraints' => new Valid(),
			'entry_type' => SiteDomainForm::class,
			'allow_add' => true,
			'allow_delete' => true,
			'by_reference' => false,
			'entry_options' => [
				'label' => false,
			],
		]);
	}

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Site\MultiSiteBundle\Entity\Site',
			'csrf_protection' => false,
        ));

        //$resolver->setRequired('em');
    }

    public function getBlockPrefix(): string
    {
        return 'site';
    }
}