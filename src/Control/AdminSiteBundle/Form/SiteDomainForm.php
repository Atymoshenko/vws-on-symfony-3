<?php
namespace Control\AdminSiteBundle\Form;

use Site\CommonBundle\Traits\RepositoryTrait;
use Doctrine\ORM\EntityManager;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ButtonType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\RadioType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Site\UserBundle\Entity\Sex;
use Site\UserBundle\Entity\UserGroup;

class SiteDomainForm extends AbstractType
{
	use RepositoryTrait;

	/** @var FormBuilderInterface */
	private $builder;

	/** @var EntityManager */
	private $em;

	protected function em(): EntityManager
	{
		return $this->em;
	}

	public function buildForm(FormBuilderInterface $builder, array $options)
    {
    	//$this->em = $options['em'];

    	$this->builder = $builder;

		$this->buildDomain();
		$this->buildDelete();
    }

	private function buildDomain()
	{
		$this->builder->add('domain', TextType::class, [
			'label' => 'Domain',
		]);
	}

	private function buildDelete()
	{
		$this->builder->add('delete', ButtonType::class, [
			'label' => 'Delete domain',
		]);
	}

	public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Site\MultiSiteBundle\Entity\SiteDomain',
			'csrf_protection' => false,
        ));

        //$resolver->setRequired('em');
    }

    public function getBlockPrefix(): string
    {
        return 'site_domain';
    }
}