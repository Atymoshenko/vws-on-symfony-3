<?php
namespace Control\AdminSiteBundle\Form\Filter;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class SitesListFilterForm extends AbstractType
{
	/** @var FormBuilderInterface */
	private $builder;

	public function buildForm(FormBuilderInterface $builder, array $options)
    {
    	$this->builder = $builder;
    	$this->builder->setMethod('GET');

        $this->buildName();
    }

    private function buildName()
	{
		$this->builder->add('name', TextType::class, [
			'label' => 'Name',
			'required' => false,
		]);
	}

	public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Control\AdminSiteBundle\Entity\Filter\SitesListFilter',
			'csrf_protection' => false,
        ));
    }

    public function getBlockPrefix(): string
    {
        return '';
    }
}