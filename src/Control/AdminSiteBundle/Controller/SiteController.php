<?php
namespace Control\AdminSiteBundle\Controller;

use Control\AdminBundle\Controller\BaseController;
use Control\AdminSiteBundle\Entity\Filter\SitesListFilter;
use Control\AdminSiteBundle\Form\Filter\SitesListFilterForm;
use Control\AdminSiteBundle\Form\SiteForm;
use Control\AdminSiteBundle\Manager\SiteManager;
use Doctrine\Common\Collections\ArrayCollection;
use Site\CommonBundle\Helpers\PagerHelper;
use Symfony\Component\Form\Form;
use Symfony\Component\Form\FormError;
use Symfony\Component\HttpFoundation\Request;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Site\MultiSiteBundle\Entity\Site;
use Site\MultiSiteBundle\Entity\SiteHistory;
use Site\MultiSiteBundle\Entity\SiteMultiSiteUser;
use Site\MultiSiteBundle\Traits\SiteMultiSiteRepositoryTrait;

/**
 * @Security("has_role('ROLE_ADMIN_SITE')")
 */
class SiteController extends BaseController
{
	use SiteMultiSiteRepositoryTrait;

	/**
	 * @Route("/sites", name="_admin_site_sites")
	 */
	public function sitesAction(Request $request)
	{
		$filter = new SitesListFilter();
		$formFilter = $this->createForm(SitesListFilterForm::class, $filter);
		$formFilter->handleRequest($request);
		if ($formFilter->isSubmitted() && $formFilter->isValid() && $filter->hasConditions()) {

		}

		$pager = new PagerHelper($this->getSiteRepository()->getFilteredListQuery($filter));
		/** @var Site[] $sites */
		$sites = $pager->getItemsByPage($request->get('p'));

		return $this->render('ControlAdminSiteBundle:Bootstrap/Site:sitesList.html.twig', ['sites' => $sites, 'pager' => $pager, 'filter' => $filter, 'formFilter' => $formFilter->createView(),]);
	}

	/**
	 * @Route("/site/create", name="_admin_site_create")
	 */
	public function siteCreateAction(Request $request)
	{
		$site = new Site();

		$form = $this->createForm(SiteForm::class, $site, [
			'action' => $this->generateUrl('_admin_site_create'),
		]);
		$form->handleRequest($request);
		if ($form->isSubmitted() && $form->isValid()) {
			try {
				if ($this->validateSiteFieldUserId($form, $site)) {
					/** @var SiteManager $manager */
					$manager = $this->get('site_manager');
					$manager->createSite($site);
					$this->addFlash('success', 'Site is created.');
					return $this->redirect($this->generateUrl('_admin_site_edit', ['id' => $site->getId()]));
				}
			} catch (\Throwable $e) {
				throw $e;
			}
		}

		return $this->render('ControlAdminSiteBundle:Bootstrap/Site:siteEdit.html.twig', ['form' => $form->createView(),]);
	}

	/**
	 * @Route("/site/edit/{id}", name="_admin_site_edit")
	 */
	public function siteEditAction(Request $request)
	{
		/** @var Site $site */
		$site = $this->getSiteRepository()->find($request->get('id'));
		if (!$site) {
			throw $this->createNotFoundException('Site not found.');
		}

		/** @var SiteMultiSiteUser $siteMultiSiteUser */
		$siteMultiSiteUser = $this->getSiteMultiSiteUserRepository()->find($this->getUser()->getId());
		$siteHistory = new SiteHistory($site, $siteMultiSiteUser);

		$domainsOriginal = new ArrayCollection();
		foreach ($site->getDomains() as $domain) {
			$domainsOriginal->add($domain);
		}

		/** @var SiteManager $manager */
		$manager = $this->get('site_manager');
		$manager->loadDatabaseParameters($site);

		$site->setSitePrevious($site);
		$form = $this->createForm(SiteForm::class, $site, [
			'action' => $this->generateUrl('_admin_site_edit',['id' => $site->getId()]),
		]);
		$form->handleRequest($request);
		if ($form->isSubmitted() && $form->isValid()) {
			if ($siteHistory->isDifferent($site) || true) {
				try {
					if ($this->validateSiteFieldUserId($form, $site)) {
						$manager->editSite($site, $domainsOriginal);
						$this->save($siteHistory);
						$this->addFlash('success', 'Site is saved.');

						return $this->redirect($this->generateUrl('_admin_site_edit', ['id' => $site->getId()]));
					}
				} catch (\Throwable $e) {
					throw $e;
				}
			}
			else {
				return $this->redirect($this->generateUrl('_admin_site_edit', ['id' => $site->getId()]));
			}
		}

		return $this->render('ControlAdminSiteBundle:Bootstrap/Site:siteEdit.html.twig', ['form' => $form->createView(),]);
	}

	private function validateSiteFieldUserId(Form $form, Site $site): bool
	{
		$userId = $form->get('userId')->getData();
		if (!$userId) {
			return true;
		}

		/** @var SiteMultiSiteUser $user */
		$user = $this->getSiteMultiSiteUserRepository()->find($userId);
		if ($user) {
			$site->setUser($user);
			return true;
		}

		$form->get('userId')->addError(new FormError('User not found.'));

		return false;
	}
}