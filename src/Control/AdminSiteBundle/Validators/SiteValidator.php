<?php
namespace Control\AdminSiteBundle\Validators;

use Symfony\Component\Validator\Context\ExecutionContextInterface;
use Site\MultiSiteBundle\Entity\Site;

class SiteValidator
{
	const DATE_PATTERN_Y_m_d_H_i = "~^\d{4}\-(0[1-9]{1}|1[0-2]{1})\-\d{2} \d{2}:\d{2}$~";

	/** @var ExecutionContextInterface */
	private $context;

	public function __construct(ExecutionContextInterface $context)
	{
		$this->context = $context;
	}

	public function validate(): void
	{
		/** @var Site $site */
		$site = $this->context->getObject();

		if (!$site->getUserId()) {
			$this->context->buildViolation("User can't be empty.")->atPath('userId')->addViolation();
		}

		if (!$site->getEnvironment()) {
			$this->context->buildViolation("Envitonment can't be empty.")->atPath('environment')->addViolation();
		}

		if (!$site->getDateExpire()) {
			$this->context->buildViolation("Date expire can't be empty.")->atPath('dateExpire')->addViolation();
		} else {
			if (is_string($site->getDateExpire())) {
				if (!preg_match(self::DATE_PATTERN_Y_m_d_H_i, $site->getDateExpire())) {
					$this->context->buildViolation("Wrong date expire.")->atPath('dateExpire')->addViolation();
				}
			}
			else {
				if (!preg_match(self::DATE_PATTERN_Y_m_d_H_i, $site->getDateExpire()->format('Y-m-d H:i'))) {
					$this->context->buildViolation("Wrong date expire.")->atPath('dateExpire')->addViolation();
				}
			}
		}
	}
}