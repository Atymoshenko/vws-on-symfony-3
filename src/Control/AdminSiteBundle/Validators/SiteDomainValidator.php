<?php
namespace Control\AdminSiteBundle\Validators;

use Symfony\Component\Validator\Context\ExecutionContextInterface;
use Site\MultiSiteBundle\Entity\SiteDomain;

class SiteDomainValidator
{
	/** @var ExecutionContextInterface */
	private $context;

	public function __construct(ExecutionContextInterface $context)
	{
		$this->context = $context;
	}

	public function validate(): void
	{
		/** @var SiteDomain $siteDomain */
		$siteDomain = $this->context->getObject();

		if (!$siteDomain->getDomain()) {
			$this->context->buildViolation("Domain can't be empty.")->atPath('domain')->addViolation();
		}

	}
}