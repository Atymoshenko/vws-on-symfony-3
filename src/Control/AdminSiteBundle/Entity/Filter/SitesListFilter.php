<?php
namespace Control\AdminSiteBundle\Entity\Filter;

use Site\CommonBundle\Entity\Filter\ListFilter;

class SitesListFilter extends ListFilter
{
	/** @var string|null */
	protected $name;

	public function getName(): ?string
	{
		return $this->name;
	}

	public function setName(?string $name)
	{
		$this->name = $name;
	}

}