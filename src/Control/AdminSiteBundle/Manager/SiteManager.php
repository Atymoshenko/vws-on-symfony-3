<?php
namespace Control\AdminSiteBundle\Manager;

use Doctrine\Common\Collections\ArrayCollection;
use Site\MultiSiteBundle\Entity\Environment;
use Site\MultiSiteBundle\Entity\Site;
use Site\MultiSiteBundle\Entity\SiteDomain;

class SiteManager extends BaseManager
{
	/** @var \mysqli */
	private $siteMysqli;

	public function createSite(Site $site): void
	{
		$this->em()->beginTransaction();
		try {
			$this->saveSite($site, false);

			if (!$this->createDatabase($site)) {
				throw new \Exception("Can't create database.");
			}
			if (!$this->createDatabaseStructure($site)) {
				throw new \Exception("Can't create database structure.");
			}

			if ($site->getEnvironment()->isLocal()) {
				$pathSitesParameters = $this->getPathSitesLocalParameters();

				if (!is_dir($pathSitesParameters)) {
					throw new \Exception("Directory $pathSitesParameters is not exists.");
				}
				if (!is_writable($pathSitesParameters)) {
					throw new \Exception("Directory $pathSitesParameters is not writable.");
				}

				$dirs = $this->getDirsSiteLocalParametersBySiteId($site->getId());
				$pathSiteParameters = $pathSitesParameters;
				foreach ($dirs as $dir) {
					$pathSiteParameters.= '/' . $dir;
					if (!is_dir($pathSiteParameters)) {
						if (!@mkdir($pathSiteParameters, 0770)) {
							throw new \Exception("Can't create directory '$pathSiteParameters'.");
						}
					}
				}
				$files = [
					'config_prod.yml.dist' => 'config_prod.yml',
					'config_dev.yml.dist' => 'config_dev.yml',
				];
				foreach ($files as $fileNameSource => $fileNameDestination) {
					if (!copy($pathSitesParameters . "/$fileNameSource", $pathSiteParameters . "/$fileNameDestination")) {
						throw new \Exception("Can't copy file {$pathSitesParameters}/{$fileNameSource} to {$pathSiteParameters}/{$fileNameDestination}");
					}
				}
				$dataParameters = file_get_contents($pathSitesParameters . '/parameters.yml.dist');
				$this->setupParameters($dataParameters, $site);
				file_put_contents($pathSiteParameters . '/parameters.yml', $dataParameters);
			}

			$this->setDomains($site);
			$this->saveSite($site);
			$this->em()->commit();
		}
		catch (\Throwable $e) {
			$this->em()->rollback();
			throw $e;
		}
	}

	public function editSite(Site $site, ArrayCollection $domainsOriginal): void
	{
		$this->em()->beginTransaction();
		$this->saveSite($site, true, $domainsOriginal);
		try {
			$pathSitesParameters = $this->getPathSitesLocalParameters();
			$dirs = $this->getDirsSiteLocalParametersBySiteId($site->getId());
			$pathSiteParameters = $pathSitesParameters;
			foreach ($dirs as $dir) {
				$pathSiteParameters.= '/' . $dir;
			}
			$dataParameters = file_get_contents($pathSiteParameters . '/parameters.yml');
			$this->setupParameters($dataParameters, $site);
			file_put_contents($pathSiteParameters . '/parameters.yml', $dataParameters);

			$this->setDomains($site, $domainsOriginal);
			$this->em()->commit();
		}
		catch (\Throwable $e) {
			$this->em()->rollback();
			throw $e;
		}
	}

	public function loadDatabaseParameters(Site $site): void
	{
		if (!$site->getEnvironment() || !$site->getEnvironment()->isLocal()) {
			return;
		}

		$pathSiteParameters = $this->getPathSitesLocalParameters() . '/' . implode('/', $this->getDirsSiteLocalParametersBySiteId($site->getId()));
		$dataParameters = file_get_contents($pathSiteParameters . '/parameters.yml');

		preg_match_all("~database_host:\s*(\S+)\n~", $dataParameters, $matchesHost);
		preg_match_all("~database_name:\s*(\S+)\n~", $dataParameters, $matchesName);
		preg_match_all("~database_user:\s*(\S+)\n~", $dataParameters, $matchesUser);
		preg_match_all("~database_password:\s*(\S+)\n~", $dataParameters, $matchesPassword);
		if (isset($matchesHost[1][0])) {
			$site->setDatabaseHost($matchesHost[1][0]);
		}
		if (isset($matchesName[1][0])) {
			$site->setDatabaseName($matchesName[1][0]);
		}
		if (isset($matchesUser[1][0])) {
			$site->setDatabaseUser($matchesUser[1][0]);
		}
		if (isset($matchesPassword[1][0])) {
			$site->setDatabasePassword($matchesPassword[1][0]);
		}
	}

	private function setupParameters(string &$dataParameters, Site $site): void
	{
		$dataParameters = preg_replace("~(database_host:).*(?=\n)~", "$1 " . $site->getDatabaseHost(), $dataParameters);
		$dataParameters = preg_replace("~(database_name:).*(?=\n)~", "$1 " . $site->getDatabaseName(), $dataParameters);
		$dataParameters = preg_replace("~(database_user:).*(?=\n)~", "$1 " . $site->getDatabaseUser(), $dataParameters);
		$dataParameters = preg_replace("~(database_password:).*(?=\n)~", "$1 " . $site->getDatabasePassword(), $dataParameters);
		$dataParameters = preg_replace("~(secret:).*(?=\n)~", "$1 " . md5($site->getId()), $dataParameters);
		$dataParameters = preg_replace("~(date_expire:).*(?=\n)~", "$1 " . $site->getDateExpire()->format('Y-m-d H:i:s'), $dataParameters);
	}

	private function setDomains(Site $site, ArrayCollection $domainsOriginal = null): void
	{
		if (
			(!$site->getEnvironment() || !$site->getEnvironment()->isLocal())
			&&
			(is_null($site->getSitePrevious()) || !$site->getSitePrevious()->getEnvironment() || !$site->getSitePrevious()->getEnvironment()->isLocal())
		) {
			return;
		}

		$pathDomains = $this->getPathDomainsLocal();
		if (!is_dir($pathDomains)) {
			throw new \Exception('Directory "' . $pathDomains . '" is not exists.');
		}
		if (!is_writable($pathDomains)) {
			throw new \Exception("Directory $pathDomains is not writable.");
		}

		if ($domainsOriginal) {
			foreach ($domainsOriginal as $domainOriginal) {
				$dirs = $this->getDirsDomainLocalByDomain($domainOriginal->getDomain());
				$pathDomain = $pathDomains . '/' . implode('/', $dirs);

				if (!$site->getDomains()->contains($domainOriginal)) {
					$domainAsFolderOriginal = /*SiteDomain::convertDomainToFolder($domainOriginal->getDomain())*/$domainOriginal->getDomain();
					$pathDomainOriginal = "{$pathDomain}/{$domainAsFolderOriginal}.php";
					$this->deleteDomainStorage($pathDomainOriginal);
					$this->clearDomainCache($domainOriginal);
				}
			}
		}

		foreach ($site->getDomains() as $domain) {
			$dirs = $this->getDirsDomainLocalByDomain($domain->getDomain());
			$pathDomain = $pathDomains;
			foreach ($dirs as $dir) {
				$pathDomain.= '/' . $dir;
				if (!is_dir($pathDomain)) {
					if (!@mkdir($pathDomain, 0770)) {
						throw new \Exception("Can't create directory '$pathDomain'.");
					}
				}
			}

			$domainAsFolder = /*SiteDomain::convertDomainToFolder($domain->getDomain())*/$domain->getDomain();
			$pathDomain = "{$pathDomain}/{$domainAsFolder}.php";
			$data = "<?php\nreturn ['siteString'=>'site_{$site->getId()}','siteDirs'=>'" . implode('/', $this->getDirsSiteLocalParametersBySiteId($site->getId())) . "'];";
			file_put_contents($pathDomain, $data);
		}

	}

	private function clearDomainCache(SiteDomain $domain): void
	{
		$domainAsFolder = /*SiteDomain::convertDomainToFolder($domain->getDomain())*/$domain->getDomain();

		$this->deleteFolder($this->getKernelCacheDir() . '/' . $domainAsFolder . '_prod');
		$this->deleteFolder($this->getKernelCacheDir() . '/' . $domainAsFolder . '_dev');
	}

	private function deleteDomainStorage($path): void
	{
		@unlink($path);
		/*if (is_dir($path)) {
			$files = glob($path . '/*');
			foreach ($files as $file) {
				$this->deleteFolder($file);
			}
			@rmdir($path);
		} else {
			@unlink($path);
		}*/
	}

	private function deleteFolder($path): void
	{
		if (is_dir($path)) {
			$files = glob($path . '/*');
			foreach ($files as $file) {
				$this->deleteFolder($file);
			}
			@rmdir($path);
		} else {
			@unlink($path);
		}
	}

	private function getPathSitesLocalParameters(): string
	{
		return $this->getKernelRootDir() . '/' . $this->getConfig()['environments'][Environment::ENVIRONMENT_LOCAL_MULTISITE]['pathSitesParameters'];
	}

	private function getPathDomainsLocal(): string
	{
		return $this->getKernelRootDir() . '/' . $this->getConfig()['environments'][Environment::ENVIRONMENT_LOCAL_MULTISITE]['pathDomains'];
	}

	private function getDirsSiteLocalParametersBySiteId(int $id): array
	{
		$dirs = [];

		$dirs[0] = intval(ceil($id / 1000000));
		$dirs[1] = intval(ceil($id / 1000));
		$dirs[2] = $id;

		return $dirs;
	}

	private function createDatabase(Site $site): bool
	{
		try {
			$this->siteMysqli = new \mysqli($site->getDatabaseHost(), 'root', 'qwerty');
			$dbName = $this->siteMysqli->escape_string($site->getDatabaseName());
			$sql = "CREATE DATABASE `$dbName` DEFAULT CHARACTER SET `utf8` COLLATE `utf8_general_ci`";
			if ($this->siteMysqli->query($sql) === true) {
				if ($this->siteMysqli->select_db($site->getDatabaseName()) === false) {
					return false;
				}

				return true;
			}

			return false;
		}
		catch (\Throwable $e) {
			return false;
		}
	}

	private function createDatabaseStructure(Site $site): bool
	{
		if (is_null($this->siteMysqli)) {
			return false;
		}

		$fileDbStructure = $this->getKernelRootDir() . '/copyForUser/db/db_structure.sql';
		if (!is_file($fileDbStructure)) {
			return false;
		}

		if (($dbStructureData = file_get_contents($fileDbStructure)) === false) {
			return false;
		}

		$sqls = preg_split("~;\r?\n~", $dbStructureData);
		unset($dbStructureData);
		if (!is_array($sqls)) {
			return false;
		}
		foreach ($sqls as $sql) {
			$sql = trim($sql);
			if ($sql === '') {
				continue;
			}

			if ($this->siteMysqli->query($sql) === false) {
				return false;
			}
		}

		return true;
	}

	static public function getDirsDomainLocalByDomain(string $domain): array
	{
		$dirs = [];

		$length = mb_strlen($domain);
		$dirs[0] = (string)mb_substr($domain, 0, 1);
		if ($length > 1) {
			$char = mb_substr($domain, 1, 1);
			if ($char === '.') {
				return $dirs;
			}
			$dirs[1] = (string)$char;
		}
		if ($length > 2) {
			$char = mb_substr($domain, 2, 1);
			if ($char === '.') {
				return $dirs;
			}
			$dirs[2] = (string)$char;
		}

		return $dirs;
	}

}