<?php
namespace Control\AdminSiteBundle\Manager;

use Doctrine\Bundle\DoctrineBundle\ConnectionFactory;
use Doctrine\ORM\EntityManager;
use Site\MultiSiteBundle\Traits\SiteMultiSiteRepositoryTrait;

class BaseManager
{
	use SiteMultiSiteRepositoryTrait;

	/** @var EntityManager */
	private $em;

	/** @var [] */
	private $config;

	/** @var string */
	private $kernelCacheDir;

	/** @var string */
	private $kernelRootDir;

	public function __construct(EntityManager $em, array $config, string $kernelCacheDir, string $kernelRootDir)
	{
		$this->em = $em;
		$this->config = $config;
		$this->kernelCacheDir = dirname($kernelCacheDir);
		$this->kernelRootDir = $kernelRootDir;
	}

	protected function em(): EntityManager
	{
		return $this->em;
	}

	protected function getConfig(): array
	{
		return $this->config;
	}

	protected function getKernelCacheDir(): string
	{
		return $this->kernelCacheDir;
	}

	public function getKernelRootDir(): string
	{
		return $this->kernelRootDir;
	}

}