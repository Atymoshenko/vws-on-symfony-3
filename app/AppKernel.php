<?php

use Symfony\Component\HttpKernel\Kernel;
use Symfony\Component\Config\Loader\LoaderInterface;

class AppKernel extends Kernel
{
	private $multisiteEnvironment;
	private $multisiteString;
	private $multisiteDirs;

	public function __construct(string $environment, bool $debug, string $domain = null)
	{
		$this->multisiteEnvironment = $environment;
		//if ($_SERVER['REMOTE_ADDR'] == '127.0.0.1') $domain = null;
		if (!isset($_SERVER['HTTP_HOST']) || preg_replace("~^www\.~", '', $_SERVER['HTTP_HOST']) == 'vws-on-symfony.local') $domain = null;
		if (!is_null($domain)) {
			$multisiteDomain = preg_replace("~^www\.~", '', $domain);
			//$multisiteDomain = $this->getFolderDomain($multisiteDomain);
			$pathDomain = $this->getRootDir() . '/domains/' . implode('/', \Control\AdminSiteBundle\Manager\SiteManager::getDirsDomainLocalByDomain($multisiteDomain));
			$pathDomain.= '/' . $multisiteDomain . '.php';
			if (!is_file($pathDomain)) {
				header('Content-Type: text/plain; charset=utf-8');
				header("HTTP/1.0 500 Internal Server Error");
				die("Let's goodbye!");
			}
			$siteData = require $pathDomain;
			$this->multisiteString = $siteData['siteString'];
			$this->multisiteDirs = $siteData['siteDirs'];
			$environment = $this->multisiteString . '_' . $this->multisiteEnvironment;
		}

		parent::__construct($environment, $debug);
	}

	private function getFolderDomain(string $domain): string
	{
		return preg_replace('~(\.|-)~', '_', $domain);
	}

	public function registerBundles()
    {
        $bundles = [
            new Symfony\Bundle\FrameworkBundle\FrameworkBundle(),
            new Symfony\Bundle\SecurityBundle\SecurityBundle(),
            new Symfony\Bundle\TwigBundle\TwigBundle(),
            new Symfony\Bundle\MonologBundle\MonologBundle(),
            new Symfony\Bundle\SwiftmailerBundle\SwiftmailerBundle(),
            new Doctrine\Bundle\DoctrineBundle\DoctrineBundle(),
			new Doctrine\Bundle\MigrationsBundle\DoctrineMigrationsBundle(),
            new Sensio\Bundle\FrameworkExtraBundle\SensioFrameworkExtraBundle(),
			new Site\CommonBundle\SiteCommonBundle(),
            new Control\AdminBundle\ControlAdminBundle(),
			new Site\UserBundle\SiteUserBundle(),
        ];

        if (in_array($this->multisiteEnvironment, ['dev', 'test'], true)) {
            $bundles[] = new Symfony\Bundle\DebugBundle\DebugBundle();
            $bundles[] = new Symfony\Bundle\WebProfilerBundle\WebProfilerBundle();
            $bundles[] = new Sensio\Bundle\DistributionBundle\SensioDistributionBundle();
            $bundles[] = new Sensio\Bundle\GeneratorBundle\SensioGeneratorBundle();
        }

        if (is_null($this->multisiteString) || true) {
			$bundles[] = new Site\MultiSiteBundle\SiteMultiSiteBundle();
			$bundles[] = new Control\AdminSiteBundle\ControlAdminSiteBundle();
		}

        return $bundles;
    }

	public function getRootDir()
    {
        return __DIR__;
    }

    public function getCacheDir()
    {
    	if (is_null($this->multisiteString)) {
			return dirname(__DIR__).'/var/cache/'.$this->getEnvironment();
		}

		return dirname(__DIR__).'/var/cache/'.$this->multisiteDirs . '/' . $this->environment;
    }

    public function getLogDir()
    {
        return dirname(__DIR__).'/var/logs';
    }

    public function registerContainerConfiguration(LoaderInterface $loader)
    {
        $loader->load($this->getRootDir().'/config/'.(!is_null($this->multisiteString) ? 'sites/'.$this->multisiteDirs.'/config_'.$this->multisiteEnvironment : 'config_'.$this->getEnvironment()).'.yml');
    }
}